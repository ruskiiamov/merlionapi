<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Client;

use Exception;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Request\Product\GetItemsImagesRequest;
use LaptopDev\MerlionApi\Request\Product\GetItemsPropertiesRequest;
use LaptopDev\MerlionApi\Request\Product\GetItemsPropertiesSectionsRequest;

class Product extends Client
{
    /**
     * @param GetItemsPropertiesRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetItemsPropertiesRequest(GetItemsPropertiesRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetItemsPropertiesSectionsRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetItemsPropertiesSectionsRequest(GetItemsPropertiesSectionsRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetItemsImagesRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetItemsImagesRequest(GetItemsImagesRequest $request): Response
    {
        return $this->sendRequest($request);
    }
}