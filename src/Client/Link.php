<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Client;

use Exception;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Request\Link\GetLinkedItemsRequest;
use LaptopDev\MerlionApi\Request\Link\SetLinkedItemsRequest;

class Link extends Client
{
    /**
     * @param SetLinkedItemsRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendSetLinkedItemsRequest(SetLinkedItemsRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetLinkedItemsRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetLinkedItemsRequest(GetLinkedItemsRequest $request): Response
    {
        return $this->sendRequest($request);
    }
}