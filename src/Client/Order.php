<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Client;

use Exception;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Request\Order\GetCommandResultDetailsRequest;
use LaptopDev\MerlionApi\Request\Order\GetCommandResultRequest;
use LaptopDev\MerlionApi\Request\Order\GetOrderLinesGtdRequest;
use LaptopDev\MerlionApi\Request\Order\GetOrderLinesRequest;
use LaptopDev\MerlionApi\Request\Order\GetOrdersListRequest;
use LaptopDev\MerlionApi\Request\Order\SetAddOrderLineCommandRequest;
use LaptopDev\MerlionApi\Request\Order\SetDeleteOrderCommandRequest;
use LaptopDev\MerlionApi\Request\Order\SetMoveOrderLineCommandRequest;
use LaptopDev\MerlionApi\Request\Order\SetOrderHeaderCommandRequest;
use LaptopDev\MerlionApi\Request\Order\SetOrderLineCommandRequest;
use LaptopDev\MerlionApi\Request\Order\SetSignOrderCommandRequest;
use LaptopDev\MerlionApi\Request\Order\UnsetSignOrderCommandRequest;

class Order extends Client
{
    /**
     * @param SetOrderHeaderCommandRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendSetOrderHeaderCommandRequest(SetOrderHeaderCommandRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param SetOrderLineCommandRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendSetOrderLineCommandRequest(SetOrderLineCommandRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param SetAddOrderLineCommandRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendSetAddOrderLineCommandRequest(SetAddOrderLineCommandRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param SetMoveOrderLineCommandRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendSetMoveOrderLineCommandRequest(SetMoveOrderLineCommandRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param SetSignOrderCommandRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendSetSignOrderCommandRequest(SetSignOrderCommandRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param UnsetSignOrderCommandRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendUnsetSignOrderCommandRequest(UnsetSignOrderCommandRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param SetDeleteOrderCommandRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendSetDeleteOrderCommandRequest(SetDeleteOrderCommandRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetCommandResultRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetCommandResultRequest(GetCommandResultRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetCommandResultDetailsRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetCommandResultDetailsRequest(GetCommandResultDetailsRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetOrdersListRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetOrdersListRequest(GetOrdersListRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetOrderLinesRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetOrderLinesRequest(GetOrderLinesRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetOrderLinesGtdRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetOrderLinesGtdRequest(GetOrderLinesGtdRequest $request): Response
    {
        return $this->sendRequest($request);
    }
}