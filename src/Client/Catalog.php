<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Client;

use Exception;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Request\Catalog\GetCatalogRequest;
use LaptopDev\MerlionApi\Request\Catalog\GetItemsAvailRequest;
use LaptopDev\MerlionApi\Request\Catalog\GetItemsBarcodesRequest;
use LaptopDev\MerlionApi\Request\Catalog\GetItemsRequest;

class Catalog extends Client
{
    /**
     * @param GetCatalogRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetCatalogRequest(GetCatalogRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetItemsRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetItemsRequest(GetItemsRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetItemsAvailRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetItemsAvailRequest(GetItemsAvailRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetItemsBarcodesRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetItemsBarcodesRequest(GetItemsBarcodesRequest $request): Response
    {
        return $this->sendRequest($request);
    }
}