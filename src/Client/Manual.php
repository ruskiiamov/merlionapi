<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Client;

use Exception;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Request\Manual\GetCertificatesRequest;
use LaptopDev\MerlionApi\Request\Manual\GetCounterAgentRequest;
use LaptopDev\MerlionApi\Request\Manual\GetCountryRequest;
use LaptopDev\MerlionApi\Request\Manual\GetCurrencyRateRequest;
use LaptopDev\MerlionApi\Request\Manual\GetEndPointDeliveryRequest;
use LaptopDev\MerlionApi\Request\Manual\GetPackingTypesRequest;
use LaptopDev\MerlionApi\Request\Manual\GetRepresentativeRequest;
use LaptopDev\MerlionApi\Request\Manual\GetShipmentAgentsRequest;
use LaptopDev\MerlionApi\Request\Manual\GetShipmentDatesRequest;
use LaptopDev\MerlionApi\Request\Manual\GetShipmentMethodsRequest;

class Manual extends Client
{
    /**
     * @param GetShipmentDatesRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetShipmentDatesRequest(GetShipmentDatesRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetShipmentMethodsRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetShipmentMethodsRequest(GetShipmentMethodsRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetShipmentAgentsRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetShipmentAgentsRequest(GetShipmentAgentsRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetCounterAgentRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetCounterAgentRequest(GetCounterAgentRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetRepresentativeRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetRepresentativeRequest(GetRepresentativeRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetEndPointDeliveryRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetEndPointDeliveryRequest(GetEndPointDeliveryRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetPackingTypesRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetPackingTypesRequest(GetPackingTypesRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetCountryRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetCountryRequest(GetCountryRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetCurrencyRateRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetCurrencyRateRequest(GetCurrencyRateRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetCertificatesRequest $request
     * @return Response
     * @throws Exception
     */
    public function sendGetCertificatesRequest(GetCertificatesRequest $request): Response
    {
        return $this->sendRequest($request);
    }
}