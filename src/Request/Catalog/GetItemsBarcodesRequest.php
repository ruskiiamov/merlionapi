<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Catalog;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Catalog\GetItemsBarcodesResponse;

/**
 * @JMS\XmlRoot("ns1:getItemsBarcodes", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetItemsBarcodesRequest extends AbstractRequest
{
    const RESPONSE = GetItemsBarcodesResponse::class;

    /**
     * @JMS\SerializedName("cat_id")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $catId;

    /**
     * @JMS\SerializedName("item_id")
     * @JMS\XmlList(entry="item")
     * @JMS\Type("array<string>")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string[]
     */
    private $itemId;

    /**
     * @return string
     */
    public function catId(): string
    {
        return $this->catId;
    }

    /**
     * @param string $catId
     * @return $this
     */
    public function setCatId(string $catId): self
    {
        $this->catId = $catId;
        return $this;
    }

    /**
     * @return string[]
     */
    public function itemId(): array
    {
        return $this->itemId;
    }

    /**
     * @param string[] $itemId
     * @return $this
     */
    public function setItemId(array $itemId): self
    {
        $this->itemId = $itemId;
        return $this;
    }

    /**
     * @param string $item
     * @return $this
     */
    public function addItemId(string $item): self
    {
        $this->itemId[] = $item;
        return $this;
    }
}