<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Catalog;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Catalog\GetItemsAvailResponse;

/**
 * @JMS\XmlRoot("ns1:getItemsAvail", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetItemsAvailRequest extends AbstractRequest
{
    const RESPONSE = GetItemsAvailResponse::class;

    /**
     * @JMS\SerializedName("cat_id")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $catId;

    /**
     * @JMS\SerializedName("shipment_method")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $shipmentMethod;

    /**
     * @JMS\SerializedName("shipment_date")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $shipmentDate;

    /**
     * @JMS\SerializedName("only_avail")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $onlyAvail;

    /**
     * @JMS\SerializedName("item_id")
     * @JMS\XmlList(entry="item")
     * @JMS\Type("array<string>")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string[]
     */
    private $itemId;

    /**
     * @return string
     */
    public function catId(): string
    {
        return $this->catId;
    }

    /**
     * @param string $catId
     * @return $this
     */
    public function setCatId(string $catId): self
    {
        $this->catId = $catId;
        return $this;
    }

    /**
     * @return string
     */
    public function shipmentMethod(): string
    {
        return $this->shipmentMethod;
    }

    /**
     * @param string $shipmentMethod
     * @return $this
     */
    public function setShipmentMethod(string $shipmentMethod): self
    {
        $this->shipmentMethod = $shipmentMethod;
        return $this;
    }

    /**
     * @return string
     */
    public function shipmentDate(): string
    {
        return $this->shipmentMethod;
    }

    /**
     * @param string $shipmentDate
     * @return $this
     */
    public function setShipmentDate(string $shipmentDate): self
    {
        $this->shipmentDate = $shipmentDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function onlyAvail(): ?string
    {
        return $this->onlyAvail;
    }

    /**
     * @param string $onlyAvail
     * @return $this
     */
    public function setOnlyAvail(string $onlyAvail): self
    {
        $this->onlyAvail = $onlyAvail;
        return $this;
    }

    /**
     * @return string[]
     */
    public function itemId(): array
    {
        return $this->itemId;
    }

    /**
     * @param string[] $itemId
     * @return $this
     */
    public function setItemId(array $itemId): self
    {
        $this->itemId = $itemId;
        return $this;
    }

    /**
     * @param string $item
     * @return $this
     */
    public function addItemId(string $item): self
    {
        $this->itemId[] = $item;
        return $this;
    }
}