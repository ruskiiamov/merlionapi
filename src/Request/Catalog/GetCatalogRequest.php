<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Catalog;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Catalog\GetCatalogResponse;

/**
 * @JMS\XmlRoot("ns1:getCatalog", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetCatalogRequest extends AbstractRequest
{
    const RESPONSE = GetCatalogResponse::class;

    /**
     * @JMS\SerializedName("cat_id")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $catId;

    /**
     * @return string
     */
    public function catId(): string
    {
        return $this->catId;
    }

    /**
     * @param string $catId
     * @return $this
     */
    public function setCatId(string $catId): self
    {
        $this->catId = $catId;
        return $this;
    }
}