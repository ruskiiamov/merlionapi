<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request;

use LaptopDev\MerlionApi\Contract\Request;
use LaptopDev\MerlionApi\Contract\Response;

abstract class AbstractRequest implements Request
{
    const RESPONSE = Response::class;

    public function responseClassName(): string
    {
        return static::RESPONSE;
    }
}