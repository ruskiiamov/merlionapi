<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Order\GetCommandResultDetailsResponse;

/**
 * @JMS\XmlRoot("ns1:getCommandResultDetails", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetCommandResultDetailsRequest extends AbstractRequest
{
    const RESPONSE = GetCommandResultDetailsResponse::class;

    /**
     * @JMS\SerializedName("operation_no")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $operationNo;

    /**
     * @return string|null
     */
    public function operationNo(): ?string
    {
        return $this->operationNo;
    }

    /**
     * @param string $operationNo
     * @return $this
     */
    public function setOperationNo(string $operationNo): self
    {
        $this->operationNo = $operationNo;
        return $this;
    }
}