<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Order\GetOrdersListResponse;

/**
 * @JMS\XmlRoot("ns1:getOrdersList", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetOrdersListRequest extends AbstractRequest
{
    const RESPONSE = GetOrdersListResponse::class;

    /**
     * @JMS\SerializedName("document_no")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $documentNo;

    /**
     * @return string|null
     */
    public function documentNo(): ?string
    {
        return $this->documentNo;
    }

    /**
     * @param string $documentNo
     * @return $this
     */
    public function setDocumentNo(string $documentNo): self
    {
        $this->documentNo = $documentNo;
        return $this;
    }
}