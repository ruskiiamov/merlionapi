<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Order\SetSignOrderCommandResponse;

/**
 * @JMS\XmlRoot("ns1:setSignOrderCommand", namespace="https://api.merlion.com/dl/mlservice3")
 */
class SetSignOrderCommandRequest extends AbstractRequest
{
    const RESPONSE = SetSignOrderCommandResponse::class;

    /**
     * @JMS\SerializedName("document_no")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $documentNo;

    /**
     * @JMS\SerializedName("sign_type")
     * @JMS\Type("integer")
     *
     * @var int|null
     */
    private $signType;

    /**
     * @return string
     */
    public function documentNo(): string
    {
        return $this->documentNo;
    }

    /**
     * @param string $documentNo
     * @return $this
     */
    public function setDocumentNo(string $documentNo): self
    {
        $this->documentNo = $documentNo;
        return $this;
    }

    /**
     * @return int|null
     */
    public function signType(): ?int
    {
        return $this->signType;
    }

    /**
     * @param int $signType
     * @return $this
     */
    public function setSignType(int $signType): self
    {
        $this->signType = $signType;
        return $this;
    }
}