<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Order\GetOrderLinesResponse;

/**
 * @JMS\XmlRoot("ns1:getOrderLines", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetOrderLinesRequest extends AbstractRequest
{
    const RESPONSE = GetOrderLinesResponse::class;

    /**
     * @JMS\SerializedName("document_no")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $documentNo;

    /**
     * @JMS\SerializedName("details")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $details;

    /**
     * @return string
     */
    public function documentNo(): string
    {
        return $this->documentNo;
    }

    /**
     * @param string $documentNo
     * @return $this
     */
    public function setDocumentNo(string $documentNo): self
    {
        $this->documentNo = $documentNo;
        return $this;
    }

    /**
     * @return string|null
     */
    public function details(): ?string
    {
        return $this->details;
    }

    /**
     * @param string $details
     * @return $this
     */
    public function setDetails(string $details): self
    {
        $this->details = $details;
        return $this;
    }
}