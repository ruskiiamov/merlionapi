<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Order\SetOrderLineCommandResponse;

/**
 * @JMS\XmlRoot("ns1:setOrderLineCommand", namespace="https://api.merlion.com/dl/mlservice3")
 */
class SetOrderLineCommandRequest extends AbstractRequest
{
    const RESPONSE = SetOrderLineCommandResponse::class;

    /**
     * @JMS\SerializedName("document_no")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $documentNo;

    /**
     * @JMS\SerializedName("item_no")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $itemNo;

    /**
     * @JMS\SerializedName("qty")
     * @JMS\Type("integer")
     * @Required
     *
     * @var int
     */
    private $qty;

    /**
     * @JMS\SerializedName("price")
     * @JMS\Type("float")
     * @Required
     *
     * @var float|null
     */
    private $price;

    /**
     * @return string
     */
    public function documentNo(): string
    {
        return $this->documentNo;
    }

    /**
     * @param string $documentNo
     * @return $this
     */
    public function setDocumentNo(string $documentNo): self
    {
        $this->documentNo = $documentNo;
        return $this;
    }

    /**
     * @return string
     */
    public function itemNo(): string
    {
        return $this->itemNo;
    }

    /**
     * @param string $itemNo
     * @return $this
     */
    public function setItemNo(string $itemNo): self
    {
        $this->itemNo = $itemNo;
        return $this;
    }

    /**
     * @return int
     */
    public function qty(): int
    {
        return $this->qty;
    }

    /**
     * @param int $qty
     * @return $this
     */
    public function setQty(int $qty): self
    {
        $this->qty = $qty;
        return $this;
    }

    /**
     * @return float|null
     */
    public function price(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;
        return $this;
    }
}