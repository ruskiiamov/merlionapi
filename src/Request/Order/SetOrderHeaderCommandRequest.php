<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Order\SetOrderHeaderCommandResponse;

/**
 * @JMS\XmlRoot("ns1:setOrderHeaderCommand", namespace="https://api.merlion.com/dl/mlservice3")
 */
class SetOrderHeaderCommandRequest extends AbstractRequest
{
    const RESPONSE = SetOrderHeaderCommandResponse::class;

    /**
     * @JMS\SerializedName("document_no")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $documentNo;

    /**
     * @JMS\SerializedName("shipment_method")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $shipmentMethod;

    /**
     * @JMS\SerializedName("shipment_date")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $shipmentDate;

    /**
     * @JMS\SerializedName("counter_agent")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $counterAgent;

    /**
     * @JMS\SerializedName("shipment_agent")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $shipmentAgent;

    /**
     * @JMS\SerializedName("end_customer")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $endCustomer;

    /**
     * @JMS\SerializedName("comment")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $comment;

    /**
     * @JMS\SerializedName("representative")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $representative;

    /**
     * @JMS\SerializedName("endpoint_delivery_id")
     * @JMS\Type("int")
     *
     * @var int|null
     */
    private $endpointDeliveryId;

    /**
     * @JMS\SerializedName("packing_type")
     * @JMS\Type("string")
     *
     * @var string|null
     */
    private $packingType;

    /**
     * @return string|null
     */
    public function documentNo(): ?string
    {
        return $this->documentNo;
    }

    /**
     * @param string $documentNo
     * @return $this
     */
    public function setDocumentNo(string $documentNo): self
    {
        $this->documentNo = $documentNo;
        return $this;
    }

    /**
     * @return string
     */
    public function shipmentMethod(): string
    {
        return $this->shipmentMethod;
    }

    /**
     * @param string $shipmentMethod
     * @return $this
     */
    public function setShipmentMethod(string $shipmentMethod): self
    {
        $this->shipmentMethod = $shipmentMethod;
        return $this;
    }

    /**
     * @return string
     */
    public function shipmentDate(): string
    {
        return $this->shipmentDate;
    }

    /**
     * @param string $shipmentDate
     * @return $this
     */
    public function setShipmentDate(string $shipmentDate): self
    {
        $this->shipmentDate = $shipmentDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function counterAgent(): ?string
    {
        return $this->counterAgent;
    }

    /**
     * @param string $counterAgent
     * @return $this
     */
    public function setCounterAgent(string $counterAgent): self
    {
        $this->counterAgent = $counterAgent;
        return $this;
    }

    /**
     * @return string|null
     */
    public function shipmentAgent(): ?string
    {
        return $this->shipmentAgent;
    }

    /**
     * @param string $shipmentAgent
     * @return $this
     */
    public function setShipmentAgent(string $shipmentAgent): self
    {
        $this->shipmentAgent = $shipmentAgent;
        return $this;
    }

    /**
     * @return string|null
     */
    public function endCustomer(): ?string
    {
        return $this->endCustomer;
    }

    /**
     * @param string $endCustomer
     * @return $this
     */
    public function setEndCustomer(string $endCustomer): self
    {
        $this->endCustomer = $endCustomer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function comment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return $this
     */
    public function setComment(string $comment): self
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return string|null
     */
    public function representative(): ?string
    {
        return $this->representative;
    }

    /**
     * @param string $representative
     * @return $this
     */
    public function setRepresentative(string $representative): self
    {
        $this->representative = $representative;
        return $this;
    }

    /**
     * @return int|null
     */
    public function endpointDeliveryId(): ?int
    {
        return $this->endpointDeliveryId;
    }

    /**
     * @param int $endpointDeliveryId
     * @return $this
     */
    public function setEndpointDeliveryId(int $endpointDeliveryId): self
    {
        $this->endpointDeliveryId = $endpointDeliveryId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function packingType(): ?string
    {
        return $this->packingType;
    }

    /**
     * @param string $packingType
     * @return $this
     */
    public function setPackingType(string $packingType): self
    {
        $this->packingType = $packingType;
        return $this;
    }
}