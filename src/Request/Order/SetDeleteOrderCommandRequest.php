<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Order\SetDeleteOrderCommandResponse;

/**
 * @JMS\XmlRoot("ns1:setDeleteOrderCommand", namespace="https://api.merlion.com/dl/mlservice3")
 */
class SetDeleteOrderCommandRequest extends AbstractRequest
{
    const RESPONSE = SetDeleteOrderCommandResponse::class;

    /**
     * @JMS\SerializedName("document_no")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $documentNo;

    /**
     * @return string
     */
    public function documentNo(): string
    {
        return $this->documentNo;
    }

    /**
     * @param string $documentNo
     * @return $this
     */
    public function setDocumentNo(string $documentNo): self
    {
        $this->documentNo = $documentNo;
        return $this;
    }
}