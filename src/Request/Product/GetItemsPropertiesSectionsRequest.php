<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Product;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Product\GetItemsPropertiesSectionsResponse;

/**
 * @JMS\XmlRoot("ns1:getItemsPropertiesSections", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetItemsPropertiesSectionsRequest extends AbstractRequest
{
    const RESPONSE = GetItemsPropertiesSectionsResponse::class;

    /**
     * @JMS\SerializedName("id")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string
     */
    private $id;

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }
}