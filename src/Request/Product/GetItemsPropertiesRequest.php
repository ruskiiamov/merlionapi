<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Product;

use DateTime;
use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Product\GetItemsPropertiesResponse;

/**
 * @JMS\XmlRoot("ns1:getItemsProperties", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetItemsPropertiesRequest extends AbstractRequest
{
    const RESPONSE = GetItemsPropertiesResponse::class;

    /**
     * @JMS\SerializedName("cat_id")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $catId;

    /**
     * @JMS\SerializedName("item_id")
     * @JMS\XmlList(entry="item")
     * @JMS\Type("array<string>")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string[]
     */
    private $itemId;

    /**
     * @JMS\SerializedName("page")
     * @JMS\Type("integer")
     *
     * @var int|null
     */
    private $page;

    /**
     * @JMS\SerializedName("rows_on_page")
     * @JMS\Type("integer")
     *
     * @var int|null
     */
    private $rowsOnPage;

    /**
     * @JMS\SerializedName("last_time_change")
     * @JMS\Type("DateTime<'Y-m-d\TH:i:s'>")
     * @JMS\XmlElement(cdata = false)
     *
     * @var DateTime|null
     */
    private $lastTimeChange;

    /**
     * @return string
     */
    public function catId(): string
    {
        return $this->catId;
    }

    /**
     * @param string $catId
     * @return $this
     */
    public function setCatId(string $catId): self
    {
        $this->catId = $catId;
        return $this;
    }

    /**
     * @return string[]
     */
    public function itemId(): array
    {
        return $this->itemId;
    }

    /**
     * @param string[] $itemId
     * @return $this
     */
    public function setItemId(array $itemId): self
    {
        $this->itemId = $itemId;
        return $this;
    }

    /**
     * @param string $item
     * @return $this
     */
    public function addItemId(string $item): self
    {
        $this->itemId[] = $item;
        return $this;
    }

    /**
     * @return int|null
     */
    public function page(): ?int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return int|null
     */
    public function rowsOnPage(): ?int
    {
        return $this->rowsOnPage;
    }

    /**
     * @param int $rowsOnPage
     * @return $this
     */
    public function setRowsOnPage(int $rowsOnPage): self
    {
        $this->rowsOnPage = $rowsOnPage;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function lastTimeChange(): ?DateTime
    {
        return $this->lastTimeChange;
    }

    /**
     * @param DateTime $lastTimeChange
     * @return $this
     */
    public function setLastTimeChange(DateTime $lastTimeChange): self
    {
        $this->lastTimeChange = $lastTimeChange;
        return $this;
    }
}