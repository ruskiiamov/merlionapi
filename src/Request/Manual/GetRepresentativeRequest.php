<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Manual\GetRepresentativeResponse;

/**
 * @JMS\XmlRoot("ns1:getRepresentative", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetRepresentativeRequest extends AbstractRequest
{
    const RESPONSE = GetRepresentativeResponse::class;

    /**
     * @JMS\SerializedName("CounterAgentCode")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $counterAgentCode;

    /**
     * @return string|null
     */
    public function counterAgentCode(): ?string
    {
        return $this->counterAgentCode;
    }

    /**
     * @param string $counterAgentCode
     * @return $this
     */
    public function setCounterAgentCode(string $counterAgentCode): self
    {
        $this->counterAgentCode = $counterAgentCode;
        return $this;
    }
}