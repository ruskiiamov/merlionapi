<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Manual\GetEndPointDeliveryResponse;

/**
 * @JMS\XmlRoot("ns1:getEndPointDelivery", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetEndPointDeliveryRequest extends AbstractRequest
{
    const RESPONSE = GetEndPointDeliveryResponse::class;

    /**
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     *
     * @var int|null
     */
    private $id;

    /**
     * @JMS\SerializedName("ShippingAgentCode")
     * @JMS\Type("string")
     *
     * @var string|null
     */
    private $shippingAgentCode;

    /**
     * @return int|null
     */
    public function id(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function shippingAgentCode(): ?string
    {
        return $this->shippingAgentCode;
    }

    /**
     * @param string $shippingAgentCode
     * @return $this
     */
    public function setShippingAgentCode(string $shippingAgentCode): self
    {
        $this->shippingAgentCode = $shippingAgentCode;
        return $this;
    }
}