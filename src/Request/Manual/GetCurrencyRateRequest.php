<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Manual\GetCurrencyRateResponse;

/**
 * @JMS\XmlRoot("ns1:getCurrencyRate", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetCurrencyRateRequest extends AbstractRequest
{
    const RESPONSE = GetCurrencyRateResponse::class;

    /**
     * @JMS\SerializedName("date")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $date;

    /**
     * @return string|null
     */
    public function date(): ?string
    {
        return $this->date;
    }

    /**
     * @param string $date
     * @return $this
     */
    public function setDate(string $date): self
    {
        $this->date = $date;
        return $this;
    }
}