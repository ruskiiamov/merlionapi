<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Manual\GetCertificatesResponse;

/**
 * @JMS\XmlRoot("ns1:getCertificates", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetCertificatesRequest extends AbstractRequest
{
    const RESPONSE = GetCertificatesResponse::class;

    /**
     * @JMS\SerializedName("item_id")
     * @JMS\Type("integer")
     * @Required
     *
     * @var int
     */
    private $itemId;

    /**
     * @JMS\SerializedName("page")
     * @JMS\Type("integer")
     *
     * @var int|null
     */
    private $page;

    /**
     * @JMS\SerializedName("rows_on_page")
     * @JMS\Type("integer")
     *
     * @var int|null
     */
    private $rowsOnPage;

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     * @return $this
     */
    public function setItemId(int $itemId): self
    {
        $this->itemId = $itemId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function page(): ?int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return int|null
     */
    public function rowsOnPage(): ?int
    {
        return $this->rowsOnPage;
    }

    /**
     * @param int $rowsOnPage
     * @return $this
     */
    public function setRowsOnPage(int $rowsOnPage): self
    {
        $this->rowsOnPage = $rowsOnPage;
        return $this;
    }
}