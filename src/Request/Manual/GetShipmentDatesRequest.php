<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Manual\GetShipmentDatesResponse;

/**
 * @JMS\XmlRoot("ns1:getShipmentDates", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetShipmentDatesRequest extends AbstractRequest
{
    const RESPONSE = GetShipmentDatesResponse::class;

    /**
     * @JMS\SerializedName("code")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $code;

    /**
     * @JMS\SerializedName("ShipmentMethodCode")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $shipmentMethodCode;

    /**
     * @return string|null
     */
    public function code(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string|null
     */
    public function shipmentMethodCode(): ?string
    {
        return $this->shipmentMethodCode;
    }

    /**
     * @param string $shipmentMethodCode
     * @return $this
     */
    public function setShipmentMethodCode(string $shipmentMethodCode): self
    {
        $this->shipmentMethodCode = $shipmentMethodCode;
        return $this;
    }
}