<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Manual\GetCounterAgentResponse;

/**
 * @JMS\XmlRoot("ns1:getCounterAgent", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetCounterAgentRequest extends AbstractRequest
{
    const RESPONSE = GetCounterAgentResponse::class;

    /**
     * @JMS\SerializedName("code")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $code;

    /**
     * @return string|null
     */
    public function code(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }
}