<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Link;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Link\SetLinkedItemsResponse;

/**
 * @JMS\XmlRoot("ns1:setLinkedItems", namespace="https://api.merlion.com/dl/mlservice3")
 */
class SetLinkedItemsRequest extends AbstractRequest
{
    const RESPONSE = SetLinkedItemsResponse::class;

    /**
     * @JMS\SerializedName("item_id")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $itemId;

    /**
     * @JMS\SerializedName("custitem_id")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $custItemId;

    /**
     * @JMS\SerializedName("custitem_name")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $custItemName;

    /**
     * @return string|null
     */
    public function itemId(): ?string
    {
        return $this->itemId;
    }

    /**
     * @param string $itemId
     * @return $this
     */
    public function setItemId(string $itemId): self
    {
        $this->itemId = $itemId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function custItemId(): ?string
    {
        return $this->custItemId;
    }

    /**
     * @param string $custItemId
     * @return $this
     */
    public function setCustItemId(string $custItemId): self
    {
        $this->custItemId = $custItemId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function custItemName(): ?string
    {
        return $this->custItemName;
    }

    /**
     * @param string $custItemName
     * @return $this
     */
    public function setCustItemName(string $custItemName): self
    {
        $this->custItemName = $custItemName;
        return $this;
    }
}