<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Request\Link;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Request\AbstractRequest;
use LaptopDev\MerlionApi\Response\Link\GetLinkedItemsResponse;

/**
 * @JMS\XmlRoot("ns1:getLinkedItems", namespace="https://api.merlion.com/dl/mlservice3")
 */
class GetLinkedItemsRequest extends AbstractRequest
{
    const RESPONSE = GetLinkedItemsResponse::class;

    /**
     * @JMS\SerializedName("page")
     * @JMS\Type("integer")
     *
     * @var int|null
     */
    private $page;

    /**
     * @JMS\SerializedName("rows_on_page")
     * @JMS\Type("integer")
     *
     * @var int|null
     */
    private $rowsOnPage;

    /**
     * @JMS\SerializedName("type_item_id")
     * @JMS\Type("integer")
     *
     * @var int|null
     */
    private $typeItemId;

    /**
     * @JMS\SerializedName("item_id")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<string>")
     *
     * @var string[]|null
     */
    private $itemId;

    /**
     * @return int|null
     */
    public function page(): ?int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return int|null
     */
    public function rowsOnPage(): ?int
    {
        return $this->rowsOnPage;
    }

    /**
     * @param int $rowsOnPage
     * @return $this
     */
    public function setRowsOnPage(int $rowsOnPage): self
    {
        $this->rowsOnPage = $rowsOnPage;
        return $this;
    }

    /**
     * @return int|null
     */
    public function typeItemId(): ?int
    {
        return $this->typeItemId;
    }

    /**
     * @param int $typeItemId
     * @return $this
     */
    public function setTypeItemId(int $typeItemId): self
    {
        $this->typeItemId = $typeItemId;
        return $this;
    }

    /**
     * @return string[]|null
     */
    public function itemId(): ?array
    {
        return $this->itemId;
    }

    /**
     * @param array $itemId
     * @return $this
     */
    public function setItemId(array $itemId): self
    {
        $this->itemId = $itemId;
        return $this;
    }

    /**
     * @param string $itemId
     * @return $this
     */
    public function addItemId(string $itemId): self
    {
        $this->itemId[] = $itemId;
        return $this;
    }
}