<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi;

use DMT\Soap\Serializer\SoapDateHandler;
use DMT\Soap\Serializer\SoapDeserializationVisitorFactory;
use DMT\Soap\Serializer\SoapMessageEventSubscriber;
use DMT\Soap\Serializer\SoapSerializationVisitorFactory;
use GuzzleHttp\Client as HttpClient;
use JMS\Serializer\EventDispatcher\EventDispatcher;
use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\SerializerBuilder;
use LaptopDev\MerlionApi\Client\Catalog;
use LaptopDev\MerlionApi\Client\Link;
use LaptopDev\MerlionApi\Client\Manual;
use LaptopDev\MerlionApi\Client\Order;
use LaptopDev\MerlionApi\Client\Product;

class MerlionApiClient
{
    /** @var Catalog */
    public $catalog;

    /** @var Product */
    public $product;

    /** @var Order */
    public $order;

    /** @var Link */
    public $link;

    /** @var Manual */
    public $manual;

    /**
     * @param string $baseUri
     * @param string $clientCode
     * @param string $login
     * @param string $password
     * @param int $timeout
     */
    public function __construct(
        string $baseUri,
        string $clientCode,
        string $login,
        string $password,
        int $timeout = 10
    ) {
        $httpClient = new HttpClient([
            'base_uri' => $baseUri,
            'timeout' => $timeout
        ]);

        $serializer = SerializerBuilder::create()
            ->setSerializationVisitor('soap', new SoapSerializationVisitorFactory())
            ->setDeserializationVisitor('soap', new SoapDeserializationVisitorFactory())
            ->configureListeners(
                function (EventDispatcher $dispatcher) {
                    $dispatcher->addSubscriber(
                        new SoapMessageEventSubscriber()
                    );
                }
            )
            ->configureHandlers(
                function (HandlerRegistry $registry) {
                    $registry->registerSubscribingHandler(new SoapDateHandler());
                }
            )
            ->build();

        $this->catalog = new Catalog($httpClient, $serializer, $clientCode, $login, $password);
        $this->product = new Product($httpClient, $serializer, $clientCode, $login, $password);
        $this->order = new Order($httpClient, $serializer, $clientCode, $login, $password);
        $this->link = new Link($httpClient, $serializer, $clientCode, $login, $password);
        $this->manual = new Manual($httpClient, $serializer, $clientCode, $login, $password);
    }
}