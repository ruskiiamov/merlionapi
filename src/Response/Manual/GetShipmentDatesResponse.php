<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\ShipmentDatesResult;

class GetShipmentDatesResponse implements Response
{
    /**
     * @JMS\SerializedName("getShipmentDatesResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\ShipmentDatesResult>")
     *
     * @var ShipmentDatesResult[]
     */
    private $getShipmentDatesResult;

    /**
     * @return ShipmentDatesResult[]
     */
    public function getShipmentDatesResult(): array
    {
        return $this->getShipmentDatesResult;
    }
}