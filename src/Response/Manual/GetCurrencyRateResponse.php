<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\CurrencyRateResult;

class GetCurrencyRateResponse implements Response
{
    /**
     * @JMS\SerializedName("getCurrencyRateResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\CurrencyRateResult>")
     *
     * @var CurrencyRateResult[]
     */
    private $getCurrencyRateResult;

    /**
     * @return CurrencyRateResult[]
     */
    public function getCurrencyRateResult(): array
    {
        return $this->getCurrencyRateResult;
    }
}