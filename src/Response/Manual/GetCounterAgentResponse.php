<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\DictionaryResult;

class GetCounterAgentResponse implements Response
{
    /**
     * @JMS\SerializedName("getCounterAgentResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\DictionaryResult>")
     *
     * @var DictionaryResult[]
     */
    private $getCounterAgentResult;

    /**
     * @return DictionaryResult[]
     */
    public function getCounterAgentResult(): array
    {
        return $this->getCounterAgentResult;
    }
}