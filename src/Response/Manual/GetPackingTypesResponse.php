<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\DictionaryResult;

class GetPackingTypesResponse implements Response
{
    /**
     * @JMS\SerializedName("getPackingTypesResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\DictionaryResult>")
     *
     * @var DictionaryResult[]
     */
    private $getPackingTypesResult;

    /**
     * @return DictionaryResult[]
     */
    public function getPackingTypesResult(): array
    {
        return $this->getPackingTypesResult;
    }
}