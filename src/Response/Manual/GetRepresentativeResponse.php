<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\RepresentativeResult;

class GetRepresentativeResponse implements Response
{
    /**
     * @JMS\SerializedName("getRepresentativeResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\RepresentativeResult>")
     *
     * @var RepresentativeResult[]
     */
    private $getRepresentativeResult;

    /**
     * @return RepresentativeResult[]
     */
    public function getRepresentativeResult(): array
    {
        return $this->getRepresentativeResult;
    }
}