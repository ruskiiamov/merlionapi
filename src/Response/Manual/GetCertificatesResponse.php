<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\CertificatesResult;

class GetCertificatesResponse implements Response
{
    /**
     * @JMS\SerializedName("getCertificatesResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\CertificatesResult>")
     *
     * @var CertificatesResult[]
     */
    private $getCertificatesResult;

    /**
     * @return CertificatesResult[]
     */
    public function getCertificatesResult(): array
    {
        return $this->getCertificatesResult;
    }
}