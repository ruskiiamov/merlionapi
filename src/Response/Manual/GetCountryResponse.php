<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\DictionaryResult;

class GetCountryResponse implements Response
{
    /**
     * @JMS\SerializedName("getCountryResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\DictionaryResult>")
     *
     * @var DictionaryResult[]
     */
    private $getCountryResult;

    /**
     * @return DictionaryResult[]
     */
    public function getCountryResult(): array
    {
        return $this->getCountryResult;
    }
}