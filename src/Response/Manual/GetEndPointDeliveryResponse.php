<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\EndPointDeliveryResult;

class GetEndPointDeliveryResponse implements Response
{
    /**
     * @JMS\SerializedName("getEndPointDeliveryResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\EndPointDeliveryResult>")
     *
     * @var EndPointDeliveryResult[]
     */
    private $getEndPointDeliveryResult;

    /**
     * @return EndPointDeliveryResult[]
     */
    public function getEndPointDeliveryResult(): array
    {
        return $this->getEndPointDeliveryResult;
    }
}