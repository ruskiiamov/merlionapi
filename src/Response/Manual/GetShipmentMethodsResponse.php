<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\ShipmentMethodsResult;

class GetShipmentMethodsResponse implements Response
{
    /**
     * @JMS\SerializedName("getShipmentMethodsResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\ShipmentMethodsResult>")
     *
     * @var ShipmentMethodsResult[]
     */
    private $getShipmentMethodsResult;

    /**
     * @return ShipmentMethodsResult[]
     */
    public function getShipmentMethodsResult(): array
    {
        return $this->getShipmentMethodsResult;
    }
}