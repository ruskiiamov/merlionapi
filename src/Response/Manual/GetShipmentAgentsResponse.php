<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Manual;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\DictionaryResult;

class GetShipmentAgentsResponse implements Response
{
    /**
     * @JMS\SerializedName("getShipmentAgentsResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\DictionaryResult>")
     *
     * @var DictionaryResult[]
     */
    private $getShipmentAgentsResult;

    /**
     * @return DictionaryResult[]
     */
    public function getShipmentAgentsResult(): array
    {
        return $this->getShipmentAgentsResult;
    }
}