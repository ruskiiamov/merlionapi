<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\CommandResult;

class GetCommandResultDetailsResponse implements Response
{
    /**
     * @JMS\SerializedName("getCommandResultDetailsResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\CommandResult>")
     *
     * @var CommandResult[]
     */
    private $getCommandResultDetailsResult;

    /**
     * @return CommandResult[]
     */
    public function getCommandResultDetailsResult(): array
    {
        return $this->getCommandResultDetailsResult;
    }
}