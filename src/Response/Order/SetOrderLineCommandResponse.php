<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;

class SetOrderLineCommandResponse implements Response
{
    /**
     * @JMS\SerializedName("setOrderLineCommandResult")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $setOrderLineCommandResult;

    /**
     * @return int
     */
    public function setOrderLineCommandResult(): int
    {
        return $this->setOrderLineCommandResult;
    }
}