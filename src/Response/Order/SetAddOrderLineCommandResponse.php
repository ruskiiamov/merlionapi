<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;

class SetAddOrderLineCommandResponse implements Response
{
    /**
     * @JMS\SerializedName("setAddOrderLineCommandResult")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $setAddOrderLineCommandResult;

    /**
     * @return int
     */
    public function setAddOrderLineCommandResult(): int
    {
        return $this->setAddOrderLineCommandResult;
    }
}