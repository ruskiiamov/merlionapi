<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\OrdersListResult;

class GetOrdersListResponse implements Response
{
    /**
     * @JMS\SerializedName("getOrdersListResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\OrdersListResult>")
     *
     * @var OrdersListResult[]
     */
    private $getOrdersListResult;

    /**
     * @return OrdersListResult[]
     */
    public function getOrdersListResult(): array
    {
        return $this->getOrdersListResult;
    }
}