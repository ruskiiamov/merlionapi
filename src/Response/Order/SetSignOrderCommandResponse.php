<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;

class SetSignOrderCommandResponse implements Response
{
    /**
     * @JMS\SerializedName("setSignOrderCommandResult")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $setSignOrderCommandResult;

    /**
     * @return int
     */
    public function setSignOrderCommandResult(): int
    {
        return $this->setSignOrderCommandResult;
    }
}