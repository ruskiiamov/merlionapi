<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\CommandResult;

class GetCommandResultResponse implements Response
{
    /**
     * @JMS\SerializedName("getCommandResultResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\CommandResult>")
     *
     * @var CommandResult[]
     */
    private $getCommandResultResult;

    /**
     * @return CommandResult[]
     */
    public function getCommandResultResult(): array
    {
        return $this->getCommandResultResult;
    }
}