<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;

class SetDeleteOrderCommandResponse implements Response
{
    /**
     * @JMS\SerializedName("setDeleteOrderCommandResult")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $setDeleteOrderCommandResult;

    /**
     * @return int
     */
    public function setDeleteOrderCommandResult(): int
    {
        return $this->setDeleteOrderCommandResult;
    }
}