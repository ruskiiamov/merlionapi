<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\OrderLinesResult;

class GetOrderLinesResponse implements Response
{
    /**
     * @JMS\SerializedName("getOrderLinesResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\OrderLinesResult>")
     *
     * @var OrderLinesResult[]
     */
    private $getOrderLinesResult;

    /**
     * @return OrderLinesResult[]
     */
    public function getOrderLinesResult(): array
    {
        return $this->getOrderLinesResult;
    }
}