<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;

class SetMoveOrderLineCommandResponse implements Response
{
    /**
     * @JMS\SerializedName("setMoveOrderLineCommandResult")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $setMoveOrderLineCommandResult;

    /**
     * @return int
     */
    public function setMoveOrderLineCommandResult(): int
    {
        return $this->setMoveOrderLineCommandResult;
    }
}