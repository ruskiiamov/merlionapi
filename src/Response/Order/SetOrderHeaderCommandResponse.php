<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;

class SetOrderHeaderCommandResponse implements Response
{
    /**
     * @JMS\SerializedName("setOrderHeaderCommandResult")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $setOrderHeaderCommandResult;

    /**
     * @return int
     */
    public function setOrderHeaderCommandResult(): int
    {
        return $this->setOrderHeaderCommandResult;
    }
}