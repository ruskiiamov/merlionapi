<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;

class UnsetSignOrderCommandResponse implements Response
{
    /**
     * @JMS\SerializedName("unsetSignOrderCommandResult")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $unsetSignOrderCommandResult;

    /**
     * @return int
     */
    public function unsetSignOrderCommandResult(): int
    {
        return $this->unsetSignOrderCommandResult;
    }
}