<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\OrderLinesGtdResult;

class GetOrderLinesGtdResponse implements Response
{
    /**
     * @JMS\SerializedName("getOrderLinesGTDResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\OrderLinesGtdResult>")
     *
     * @var OrderLinesGtdResult[]
     */
    private $getOrderLinesGtdResult;

    /**
     * @return OrderLinesGtdResult[]
     */
    public function getOrderLinesGtdResult(): array
    {
        return $this->getOrderLinesGtdResult;
    }
}