<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Link;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;

class SetLinkedItemsResponse implements Response
{
    /**
     * @JMS\SerializedName("setLinkedItemsResult")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $setLinkedItemsResult;

    /**
     * @return string
     */
    public function setLinkedItemsResult(): string
    {
        return $this->setLinkedItemsResult;
    }
}