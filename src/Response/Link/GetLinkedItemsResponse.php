<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Link;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\LinkedItemsResult;

class GetLinkedItemsResponse implements Response
{
    /**
     * @JMS\SerializedName("getLinkedItemsResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\LinkedItemsResult>")
     *
     * @var LinkedItemsResult[]
     */
    private $getLinkedItemsResult;

    /**
     * @return LinkedItemsResult[]
     */
    public function getLinkedItemsResult(): array
    {
        return $this->getLinkedItemsResult;
    }
}