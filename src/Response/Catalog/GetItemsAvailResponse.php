<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Catalog;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\ItemsAvailResult;

class GetItemsAvailResponse implements Response
{
    /**
     * @JMS\SerializedName("getItemsAvailResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\ItemsAvailResult>")
     *
     * @var ItemsAvailResult[]
     */
    private $getItemsAvailResult;

    /**
     * @return ItemsAvailResult[]
     */
    public function getItemsAvailResult(): array
    {
        return $this->getItemsAvailResult;
    }
}