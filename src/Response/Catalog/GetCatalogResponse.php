<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Catalog;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\CatalogResult;

class GetCatalogResponse implements Response
{
    /**
     * @JMS\SerializedName("getCatalogResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\CatalogResult>")
     *
     * @var CatalogResult[]
     */
    private $getCatalogResult;

    /**
     * @return CatalogResult[]
     */
    public function getCatalogResult(): array
    {
        return $this->getCatalogResult;
    }
}