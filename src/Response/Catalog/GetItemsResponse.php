<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Catalog;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\ItemsResult;

class GetItemsResponse implements Response
{
    /**
     * @JMS\SerializedName("getItemsResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\ItemsResult>")
     *
     * @var ItemsResult[]
     */
    private $getItemsResult;

    /**
     * @return ItemsResult[]
     */
    public function getItemsResult(): array
    {
        return $this->getItemsResult;
    }
}