<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Catalog;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\ItemsBarcodesResult;

class GetItemsBarcodesResponse implements Response
{
    /**
     * @JMS\SerializedName("getItemsBarcodesResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\ItemsBarcodesResult>")
     *
     * @var ItemsBarcodesResult[]
     */
    private $getItemsBarcodesResult;

    /**
     * @return ItemsBarcodesResult[]
     */
    public function getItemsBarcodesResult(): array
    {
        return $this->getItemsBarcodesResult;
    }
}