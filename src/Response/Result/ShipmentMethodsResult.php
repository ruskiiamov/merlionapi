<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class ShipmentMethodsResult implements Result
{
    /**
     * @JMS\SerializedName("Code")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $code;

    /**
     * @JMS\SerializedName("Description")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $description;

    /**
     * @JMS\SerializedName("IsDefault")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $isDefault;

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function isDefault(): int
    {
        return $this->isDefault;
    }
}