<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class OrderLinesGtdResult implements Result
{
    /**
     * @JMS\SerializedName("document_no")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $documentNo;

    /**
     * @JMS\SerializedName("item_no")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $itemNo;

    /**
     * @JMS\SerializedName("bundle_item_no")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $bundleItemNo;

    /**
     * @JMS\SerializedName("gtd_no")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $gtdNo;

    /**
     * @JMS\SerializedName("used_qty")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $usedQty;

    /**
     * @JMS\SerializedName("country")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $country;

    /**
     * @return string
     */
    public function documentNo(): string
    {
        return $this->documentNo;
    }

    /**
     * @return string
     */
    public function itemNo(): string
    {
        return $this->itemNo;
    }

    /**
     * @return string
     */
    public function bundleItemNo(): string
    {
        return $this->bundleItemNo;
    }

    /**
     * @return string
     */
    public function gtdNo(): string
    {
        return $this->gtdNo;
    }

    /**
     * @return int
     */
    public function usedQty(): int
    {
        return $this->usedQty;
    }

    /**
     * @return string
     */
    public function country(): string
    {
        return $this->country;
    }
}