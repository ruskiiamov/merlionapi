<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class RepresentativeResult implements Result
{
    /**
     * @JMS\SerializedName("Representative")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $representative;

    /**
     * @JMS\SerializedName("CounterAgentCode")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $counterAgentCode;

    /**
     * @JMS\SerializedName("StartDate")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $startDate;

    /**
     * @JMS\SerializedName("EndDate")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $endDate;

    /**
     * @return string
     */
    public function representative(): string
    {
        return $this->representative;
    }

    /**
     * @return string
     */
    public function counterAgentCode(): string
    {
        return $this->counterAgentCode;
    }

    /**
     * @return string
     */
    public function startDate(): string
    {
        return $this->startDate;
    }

    /**
     * @return string
     */
    public function endDate(): string
    {
        return $this->endDate;
    }
}