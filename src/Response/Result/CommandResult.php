<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class CommandResult implements Result
{
    /**
     * @JMS\SerializedName("operation_no")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $operationNo;

    /**
     * @JMS\SerializedName("item_no")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $itemNo;

    /**
     * @JMS\SerializedName("CreateTime")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $createTime;

    /**
     * @JMS\SerializedName("ProcessingTime")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $processingTime;

    /**
     * @JMS\SerializedName("EndingTime")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $endingTime;

    /**
     * @JMS\SerializedName("ProcessingResult")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $processingResult;

    /**
     * @JMS\SerializedName("DocumentNo")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $documentNo;

    /**
     * @JMS\SerializedName("DocumentNo2")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $documentNo2;

    /**
     * @JMS\SerializedName("ProcessingResultComment")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $processingResultComment;

    /**
     * @JMS\SerializedName("ErrorText")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $errorText;

    /**
     * @JMS\SerializedName("ProcessingReserved")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $processingReserved;

    /**
     * @JMS\SerializedName("OperationLineNo")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $operationLineNo;

    /**
     * @return int
     */
    public function operationNo(): int
    {
        return $this->operationNo;
    }

    /**
     * @return string
     */
    public function itemNo(): string
    {
        return $this->itemNo;
    }

    /**
     * @return string
     */
    public function createTime(): string
    {
        return $this->createTime;
    }

    /**
     * @return string
     */
    public function processingTime(): string
    {
        return $this->processingTime;
    }

    /**
     * @return string
     */
    public function endingTime(): string
    {
        return $this->endingTime;
    }

    /**
     * @return string
     */
    public function processingResult(): string
    {
        return $this->processingResult;
    }

    /**
     * @return string
     */
    public function documentNo(): string
    {
        return $this->documentNo;
    }

    /**
     * @return string
     */
    public function documentNo2(): string
    {
        return $this->documentNo2;
    }

    /**
     * @return string
     */
    public function processingResultComment(): string
    {
        return $this->processingResultComment;
    }

    /**
     * @return string
     */
    public function errorText(): string
    {
        return $this->errorText;
    }

    /**
     * @return string
     */
    public function processingReserved(): string
    {
        return $this->processingReserved;
    }

    /**
     * @return int
     */
    public function operationLineNo(): int
    {
        return $this->operationLineNo;
    }
}