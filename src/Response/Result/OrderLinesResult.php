<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class OrderLinesResult implements Result
{
    /**
     * @JMS\SerializedName("item_no")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $itemNo;

    /**
     * @JMS\SerializedName("document_no")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $documentNo;

    /**
     * @JMS\SerializedName("qty")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $qty;

    /**
     * @JMS\SerializedName("desire_qty")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $desireQty;

    /**
     * @JMS\SerializedName("shipped_qty")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $shippedQty;

    /**
     * @JMS\SerializedName("price")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $price;

    /**
     * @JMS\SerializedName("amount")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $amount;

    /**
     * @JMS\SerializedName("desire_price")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $desirePrice;

    /**
     * @JMS\SerializedName("weight")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $weight;

    /**
     * @JMS\SerializedName("volume")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $volume;

    /**
     * @JMS\SerializedName("ReserveTime")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $reserveTime;

    /**
     * @return string
     */
    public function itemNo(): string
    {
        return $this->itemNo;
    }

    /**
     * @return string
     */
    public function documentNo(): string
    {
        return $this->documentNo;
    }

    /**
     * @return int
     */
    public function qty(): int
    {
        return $this->qty;
    }

    /**
     * @return int
     */
    public function desireQty(): int
    {
        return $this->desireQty;
    }

    /**
     * @return int
     */
    public function shippedQty(): int
    {
        return $this->shippedQty;
    }

    /**
     * @return float
     */
    public function price(): float
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function amount(): float
    {
        return $this->amount;
    }

    /**
     * @return float
     */
    public function desirePrice(): float
    {
        return $this->desirePrice;
    }

    /**
     * @return float
     */
    public function weight(): float
    {
        return $this->weight;
    }

    /**
     * @return float
     */
    public function volume(): float
    {
        return $this->volume;
    }

    /**
     * @return float
     */
    public function reserveTime(): float
    {
        return $this->reserveTime;
    }
}