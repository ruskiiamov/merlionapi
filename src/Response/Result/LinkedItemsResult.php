<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class LinkedItemsResult implements Result
{
    /**
     * @JMS\SerializedName("No")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $no;

    /**
     * @JMS\SerializedName("Cust_ItemNo")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $custItemNo;

    /**
     * @JMS\SerializedName("Cust_ItemName")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $custItemName;

    /**
     * @return string
     */
    public function no(): string
    {
        return $this->no;
    }

    /**
     * @return string
     */
    public function custItemNo(): string
    {
        return $this->custItemNo;
    }

    /**
     * @return string
     */
    public function custItemName(): string
    {
        return $this->custItemName;
    }
}