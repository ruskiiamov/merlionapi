<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class EndPointDeliveryResult implements Result
{
    /**
     * @JMS\SerializedName("ID")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $id;

    /**
     * @JMS\SerializedName("Endpoint_address")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $endpointAddress;

    /**
     * @JMS\SerializedName("Endpoint_contact")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $endpointContact;

    /**
     * @JMS\SerializedName("ShippingAgentCode")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $shippingAgentCode;

    /**
     * @JMS\SerializedName("City")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $city;

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function endpointAddress(): string
    {
        return $this->endpointAddress;
    }

    /**
     * @return string
     */
    public function endpointContact(): string
    {
        return $this->endpointContact;
    }

    /**
     * @return string
     */
    public function shippingAgentCode(): string
    {
        return $this->shippingAgentCode;
    }

    /**
     * @return string
     */
    public function city(): string
    {
        return $this->city;
    }
}