<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class ItemsAvailResult implements Result
{
    /**
     * @JMS\SerializedName("No")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $no;

    /**
     * @JMS\SerializedName("PriceClient")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $priceClient;

    /**
     * @JMS\SerializedName("PriceClient_RG")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $priceClientRg;

    /**
     * @JMS\SerializedName("PriceClient_MSK")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $priceClientMsk;

    /**
     * @JMS\SerializedName("AvailableClient")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $availableClient;

    /**
     * @JMS\SerializedName("AvailableClient_RG")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $availableClientRg;

    /**
     * @JMS\SerializedName("AvailableClient_MSK")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $availableClientMsk;

    /**
     * @JMS\SerializedName("AvailableExpected")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $availableExpected;

    /**
     * @JMS\SerializedName("AvailableExpectedNext")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $availableExpectedNext;

    /**
     * @JMS\SerializedName("DateExpectedNext")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $dateExpectedNext;

    /**
     * @JMS\SerializedName("RRP")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $rrp;

    /**
     * @JMS\SerializedName("RRP_Date")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $rrpDate;

    /**
     * @JMS\SerializedName("PriceClientRUB")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $priceClientRub;

    /**
     * @JMS\SerializedName("PriceClientRUB_RG")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $priceClientRubRg;

    /**
     * @JMS\SerializedName("PriceClientRUB_MSK")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $priceClientRubMsk;

    /**
     * @JMS\SerializedName("Online_Reserve")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $onlineReserve;

    /**
     * @JMS\SerializedName("ReserveCost")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $reserveCost;

    /**
     * @return string
     */
    public function no(): string
    {
        return $this->no;
    }

    /**
     * @return float
     */
    public function priceClient(): float
    {
        return $this->priceClient;
    }

    /**
     * @return float
     */
    public function priceClientRg(): float
    {
        return $this->priceClientRg;
    }

    /**
     * @return float
     */
    public function priceClientMsk(): float
    {
        return $this->priceClientMsk;
    }

    /**
     * @return int
     */
    public function availableClient(): int
    {
        return $this->availableClient;
    }

    /**
     * @return int
     */
    public function availableClientRg(): int
    {
        return $this->availableClientRg;
    }

    /**
     * @return int
     */
    public function availableClientMsk(): int
    {
        return $this->availableClientMsk;
    }

    /**
     * @return int
     */
    public function availableExpected(): int
    {
        return $this->availableExpected;
    }

    /**
     * @return int
     */
    public function availableExpectedNext(): int
    {
        return $this->availableExpectedNext;
    }

    /**
     * @return string
     */
    public function dateExpectedNext(): string
    {
        return $this->dateExpectedNext;
    }

    /**
     * @return float
     */
    public function rrp(): float
    {
        return $this->rrp;
    }

    /**
     * @return string
     */
    public function rrpDate(): string
    {
        return $this->rrpDate;
    }

    /**
     * @return float
     */
    public function priceClientRub(): float
    {
        return $this->priceClientRub;
    }

    /**
     * @return float
     */
    public function priceClientRubRg(): float
    {
        return $this->priceClientRubRg;
    }

    /**
     * @return float
     */
    public function priceClientRubMsk(): float
    {
        return $this->priceClientRubMsk;
    }

    /**
     * @return int
     */
    public function onlineReserve(): int
    {
        return $this->onlineReserve;
    }

    /**
     * @return float
     */
    public function reserveCost(): float
    {
        return $this->reserveCost;
    }
}