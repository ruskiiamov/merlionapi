<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use DateTime;
use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class ItemsResult implements Result
{
    /**
     * @JMS\SerializedName("No")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $no;

    /**
     * @JMS\SerializedName("Name")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $name;

    /**
     * @JMS\SerializedName("Brand")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $brand;

    /**
     * @JMS\SerializedName("Vendor_part")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $vendorPart;

    /**
     * @JMS\SerializedName("Size")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $size;

    /**
     * @JMS\SerializedName("EOL")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $eol;

    /**
     * @JMS\SerializedName("Warranty")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $warranty;

    /**
     * @JMS\SerializedName("Weight")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $weight;

    /**
     * @JMS\SerializedName("Volume")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $volume;

    /**
     * @JMS\SerializedName("Min_Packaged")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $minPackaged;

    /**
     * @JMS\SerializedName("Sales_Limit_Type")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $salesLimitType;

    /**
     * @JMS\SerializedName("GroupName1")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $groupName1;

    /**
     * @JMS\SerializedName("GroupName2")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $groupName2;

    /**
     * @JMS\SerializedName("GroupName3")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $groupName3;

    /**
     * @JMS\SerializedName("GroupCode1")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $groupCode1;

    /**
     * @JMS\SerializedName("GroupCode2")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $groupCode2;

    /**
     * @JMS\SerializedName("GroupCode3")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $groupCode3;

    /**
     * @JMS\SerializedName("IsBundle")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $isBundle;

    /**
     * @JMS\SerializedName("ActionDesc")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $actionDesc;

    /**
     * @JMS\SerializedName("ActionWWW")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $actionWww;

    /**
     * @JMS\SerializedName("Last_time_modified")
     * @JMS\Type("DateTime<'Y-m-d\TH:i:s'>")
     *
     * @var DateTime
     */
    private $lastTimeModified;

    /**
     * @JMS\SerializedName("VAT")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $vat;

    /**
     * @JMS\SerializedName("IsNew")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $isNew;

    /**
     * @JMS\SerializedName("Length")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $length;

    /**
     * @JMS\SerializedName("Width")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $width;

    /**
     * @JMS\SerializedName("Height")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $height;

    /**
     * @return string
     */
    public function no(): string
    {
        return $this->no;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function brand(): string
    {
        return $this->brand;
    }

    /**
     * @return string
     */
    public function vendorPart(): string
    {
        return $this->vendorPart;
    }

    /**
     * @return int
     */
    public function eol(): int
    {
        return $this->eol;
    }

    /**
     * @return int
     */
    public function warranty(): int
    {
        return $this->warranty;
    }

    /**
     * @return float
     */
    public function weight(): float
    {
        return $this->weight;
    }

    /**
     * @return float
     */
    public function volume(): float
    {
        return $this->volume;
    }

    /**
     * @return int
     */
    public function minPackaged(): int
    {
        return $this->minPackaged;
    }

    /**
     * @return string
     */
    public function salesLimitType(): string
    {
        return $this->salesLimitType;
    }

    /**
     * @return string
     */
    public function groupName1(): string
    {
        return $this->groupName1;
    }

    /**
     * @return string
     */
    public function groupName2(): string
    {
        return $this->groupName2;
    }

    /**
     * @return string
     */
    public function groupName3(): string
    {
        return $this->groupName3;
    }

    /**
     * @return string
     */
    public function groupCode1(): string
    {
        return $this->groupCode1;
    }

    /**
     * @return string
     */
    public function groupCode2(): string
    {
        return $this->groupCode2;
    }

    /**
     * @return string
     */
    public function groupCode3(): string
    {
        return $this->groupCode3;
    }

    /**
     * @return int
     */
    public function isBundle(): int
    {
        return $this->isBundle;
    }

    /**
     * @return string
     */
    public function actionDesc(): string
    {
        return $this->actionDesc;
    }

    /**
     * @return string
     */
    public function actionWww(): string
    {
        return $this->actionWww;
    }

    /**
     * @return DateTime
     */
    public function lastTimeModified(): DateTime
    {
        return $this->lastTimeModified;
    }

    /**
     * @return string
     */
    public function vat(): string
    {
        return $this->vat;
    }

    /**
     * @return int
     */
    public function isNew(): int
    {
        return $this->isNew;
    }

    /**
     * @return float
     */
    public function length(): float
    {
        return $this->length;
    }

    /**
     * @return float
     */
    public function width(): float
    {
        return $this->width;
    }

    /**
     * @return float
     */
    public function height(): float
    {
        return $this->height;
    }
}