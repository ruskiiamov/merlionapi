<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class OrdersListResult implements Result
{
    /**
     * @JMS\SerializedName("document_no")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $documentNo;

    /**
     * @JMS\SerializedName("PostedDocumentNo")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $postedDocumentNo;

    /**
     * @JMS\SerializedName("TNN")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $tnn;

    /**
     * @JMS\SerializedName("OrderDate")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $orderDate;

    /**
     * @JMS\SerializedName("Manager")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $manager;

    /**
     * @JMS\SerializedName("Contact")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $contact;

    /**
     * @JMS\SerializedName("ShipmentMethod")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $shipmentMethod;

    /**
     * @JMS\SerializedName("ShipmentMethodCode")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $shipmentMethodCode;

    /**
     * @JMS\SerializedName("ShipmentDate")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $shipmentDate;

    /**
     * @JMS\SerializedName("ActualShipmentDate")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $actualShipmentDate;

    /**
     * @JMS\SerializedName("CounterpartyClient")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $counterpartyClient;

    /**
     * @JMS\SerializedName("CounterpartyClientCode")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $counterpartyClientCode;

    /**
     * @JMS\SerializedName("ShippingAgent")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $shippingAgent;

    /**
     * @JMS\SerializedName("ShippingAgentCode")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $shippingAgentCode;

    /**
     * @JMS\SerializedName("EndCustomer")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $endCustomer;

    /**
     * @JMS\SerializedName("PostingDescription")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $postingDescription;

    /**
     * @JMS\SerializedName("Weight")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $weight;

    /**
     * @JMS\SerializedName("Volume")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $volume;

    /**
     * @JMS\SerializedName("Amount")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $amount;

    /**
     * @JMS\SerializedName("AmountRUR")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $amountRur;

    /**
     * @JMS\SerializedName("WillDeleteTomorrow")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $willDeleteTomorrow;

    /**
     * @JMS\SerializedName("Status")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $status;

    /**
     * @JMS\SerializedName("EndPointCity")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $endPointCity;

    /**
     * @JMS\SerializedName("EndPointAdress")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $endPointAdress;

    /**
     * @JMS\SerializedName("EndPointContact")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $endPointContact;

    /**
     * @JMS\SerializedName("PackingType")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $packingType;

    /**
     * @JMS\SerializedName("Representative")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $representative;

    /**
     * @JMS\SerializedName("InvoiceNo")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $invoiceNo;

    /**
     * @JMS\SerializedName("ContractId")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $contractId;

    /**
     * @return string
     */
    public function documentNo(): string
    {
        return $this->documentNo;
    }

    /**
     * @return string
     */
    public function postedDocumentNo(): string
    {
        return $this->postedDocumentNo;
    }

    /**
     * @return string
     */
    public function tnn(): string
    {
        return $this->tnn;
    }

    /**
     * @return string
     */
    public function orderDate(): string
    {
        return $this->orderDate;
    }

    /**
     * @return string
     */
    public function manager(): string
    {
        return $this->manager;
    }

    /**
     * @return string
     */
    public function contact(): string
    {
        return $this->contact;
    }

    /**
     * @return string
     */
    public function shipmentMethod(): string
    {
        return $this->shipmentMethod;
    }

    /**
     * @return string
     */
    public function shipmentMethodCode(): string
    {
        return $this->shipmentMethodCode;
    }

    /**
     * @return string
     */
    public function shipmentDate(): string
    {
        return $this->shipmentDate;
    }

    /**
     * @return string
     */
    public function actualShipmentDate(): string
    {
        return $this->actualShipmentDate;
    }

    /**
     * @return string
     */
    public function counterpartyClient(): string
    {
        return $this->counterpartyClient;
    }

    /**
     * @return string
     */
    public function counterpartyClientCode(): string
    {
        return $this->counterpartyClientCode;
    }

    /**
     * @return string
     */
    public function shippingAgent(): string
    {
        return $this->shippingAgent;
    }

    /**
     * @return string
     */
    public function shippingAgentCode(): string
    {
        return $this->shippingAgentCode;
    }

    /**
     * @return string
     */
    public function endCustomer(): string
    {
        return $this->endCustomer;
    }

    /**
     * @return string
     */
    public function postingDescription(): string
    {
        return $this->postingDescription;
    }

    /**
     * @return string
     */
    public function weight(): string
    {
        return $this->weight;
    }

    /**
     * @return float
     */
    public function volume(): float
    {
        return $this->volume;
    }

    /**
     * @return float
     */
    public function amount(): float
    {
        return $this->amount;
    }

    /**
     * @return float
     */
    public function amountRur(): float
    {
        return $this->amountRur;
    }

    /**
     * @return string
     */
    public function willDeleteTomorrow(): string
    {
        return $this->willDeleteTomorrow;
    }

    /**
     * @return string
     */
    public function status(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function endPointCity(): string
    {
        return $this->endPointCity;
    }

    /**
     * @return string
     */
    public function endPointAdress(): string
    {
        return $this->endPointAdress;
    }

    /**
     * @return string
     */
    public function endPointContact(): string
    {
        return $this->endPointContact;
    }

    /**
     * @return string
     */
    public function packingType(): string
    {
        return $this->packingType;
    }

    /**
     * @return string
     */
    public function representative(): string
    {
        return $this->representative;
    }

    /**
     * @return string
     */
    public function invoiceNo(): string
    {
        return $this->invoiceNo;
    }

    /**
     * @return string
     */
    public function contractId(): string
    {
        return $this->contractId;
    }
}