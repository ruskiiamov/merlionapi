<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class CertificatesResult implements Result
{
    /**
     * @JMS\SerializedName("Link")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $link;

    /**
     * @JMS\SerializedName("ItemNo")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $itemNo;

    /**
     * @JMS\SerializedName("CertCode")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $certCode;

    /**
     * @JMS\SerializedName("AttachNum")
     * @JMS\Type("integer")
     *
     * @var int
     */
    private $attachNum;

    /**
     * @JMS\SerializedName("ItemName")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $itemName;

    /**
     * @JMS\SerializedName("CertName")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $certName;

    /**
     * @JMS\SerializedName("Issued")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $issued;

    /**
     * @JMS\SerializedName("Expires")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $expires;

    /**
     * @JMS\SerializedName("Issuer")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $issuer;

    /**
     * @return string
     */
    public function link(): string
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function itemNo(): string
    {
        return $this->itemNo;
    }

    /**
     * @return string
     */
    public function certCode(): string
    {
        return $this->certCode;
    }

    /**
     * @return int
     */
    public function attachNum(): int
    {
        return $this->attachNum;
    }

    /**
     * @return string
     */
    public function itemName(): string
    {
        return $this->itemName;
    }

    /**
     * @return string
     */
    public function certName(): string
    {
        return $this->certName;
    }

    /**
     * @return string
     */
    public function issued(): string
    {
        return $this->issued;
    }

    /**
     * @return string
     */
    public function expires(): string
    {
        return $this->expires;
    }

    /**
     * @return string
     */
    public function issuer(): string
    {
        return $this->issuer;
    }
}