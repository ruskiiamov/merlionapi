<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class ItemsBarcodesResult implements Result
{
    /**
     * @JMS\SerializedName("ItemNo")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $itemNo;

    /**
     * @JMS\SerializedName("UnitOfMeasure")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $unitOfMeasure;

    /**
     * @JMS\SerializedName("Barcode")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $barcode;

    /**
     * @JMS\SerializedName("Checked")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $checked;

    /**
     * @return string
     */
    public function itemNo(): string
    {
        return $this->itemNo;
    }

    /**
     * @return string
     */
    public function unitOfMeasure(): string
    {
        return $this->unitOfMeasure;
    }

    /**
     * @return string
     */
    public function barcode(): string
    {
        return $this->barcode;
    }

    /**
     * @return int
     */
    public function checked(): int
    {
        return $this->checked;
    }
}