<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class ItemsImagesResult implements Result
{
    /**
     * @JMS\SerializedName("No")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $no;

    /**
     * @JMS\SerializedName("ViewType")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $viewType;

    /**
     * @JMS\SerializedName("SizeType")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $sizeType;

    /**
     * @JMS\SerializedName("FileName")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $fileName;

    /**
     * @JMS\SerializedName("Created")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $created;

    /**
     * @JMS\SerializedName("Size")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $size;

    /**
     * @JMS\SerializedName("Width")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $width;

    /**
     * @JMS\SerializedName("Height")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $height;

    /**
     * @return string
     */
    public function no(): string
    {
        return $this->no;
    }

    /**
     * @return string
     */
    public function viewType(): string
    {
        return $this->viewType;
    }

    /**
     * @return string
     */
    public function sizeType(): string
    {
        return $this->sizeType;
    }

    /**
     * @return string
     */
    public function fileName(): string
    {
        return $this->fileName;
    }

    /**
     * @return string
     */
    public function created(): string
    {
        return $this->created;
    }

    /**
     * @return int
     */
    public function size(): int
    {
        return $this->size;
    }

    /**
     * @return int
     */
    public function width(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function height(): int
    {
        return $this->height;
    }
}