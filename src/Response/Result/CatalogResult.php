<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class CatalogResult implements Result
{
    /**
     * @JMS\SerializedName("ID")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $id;

    /**
     * @JMS\SerializedName("ID_PARENT")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $idParent;

    /**
     * @JMS\SerializedName("Description")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $description;

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function idParent(): string
    {
        return $this->idParent;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }
}