<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class DictionaryResult implements Result
{
    /**
     * @JMS\SerializedName("Code")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $code;

    /**
     * @JMS\SerializedName("Description")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $description;

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }
}