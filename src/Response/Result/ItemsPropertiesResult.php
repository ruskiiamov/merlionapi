<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use DateTime;
use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class ItemsPropertiesResult implements Result
{
    /**
     * @JMS\SerializedName("No")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $no;

    /**
     * @JMS\SerializedName("PropertyID")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $propertyId;

    /**
     * @JMS\SerializedName("PropertyName")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $propertyName;

    /**
     * @JMS\SerializedName("Sorting")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $sorting;

    /**
     * @JMS\SerializedName("Value")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $value;

    /**
     * @JMS\SerializedName("Measure_Name")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $measureName;

    /**
     * @JMS\SerializedName("Last_time_modified")
     * @JMS\Type("DateTime<'Y-m-d\TH:i:s'>")
     *
     * @var DateTime
     */
    private $lastTimeModified;

    /**
     * @JMS\SerializedName("Section_Id")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $sectionId;

    /**
     * @return string
     */
    public function no(): string
    {
        return $this->no;
    }

    /**
     * @return int
     */
    public function propertyId(): int
    {
        return $this->propertyId;
    }

    /**
     * @return string
     */
    public function propertyName(): string
    {
        return $this->propertyName;
    }

    /**
     * @return int
     */
    public function sorting(): int
    {
        return $this->sorting;
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function measureName(): string
    {
        return $this->measureName;
    }

    /**
     * @return DateTime
     */
    public function lastTimeModified(): DateTime
    {
        return $this->lastTimeModified;
    }

    /**
     * @return string
     */
    public function sectionId(): string
    {
        return $this->sectionId;
    }
}