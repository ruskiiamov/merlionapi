<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class ItemsPropertiesSectionsResult implements Result
{
    /**
     * @JMS\SerializedName("id")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $id;

    /**
     * @JMS\SerializedName("name")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $name;

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}