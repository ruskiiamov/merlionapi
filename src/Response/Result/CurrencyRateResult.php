<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class CurrencyRateResult implements Result
{
    /**
     * @JMS\SerializedName("Code")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $code;

    /**
     * @JMS\SerializedName("Date")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $date;

    /**
     * @JMS\SerializedName("ExchangeRate")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $exchangeRate;

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function date(): string
    {
        return $this->date;
    }

    /**
     * @return float
     */
    public function exchangeRate(): float
    {
        return $this->exchangeRate;
    }
}