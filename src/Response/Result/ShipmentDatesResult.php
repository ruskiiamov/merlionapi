<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Result;

class ShipmentDatesResult implements Result
{
    /**
     * @JMS\SerializedName("Date")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $date;

    /**
     * @return string
     */
    public function date(): string
    {
        return $this->date;
    }
}