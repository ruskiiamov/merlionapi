<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Product;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\ItemsImagesResult;

class GetItemsImagesResponse implements Response
{
    /**
     * @JMS\SerializedName("getItemsImagesResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\ItemsImagesResult>")
     *
     * @var ItemsImagesResult[]
     */
    private $getItemsImagesResult;

    /**
     * @return ItemsImagesResult[]
     */
    public function getItemsImagesResult(): array
    {
        return $this->getItemsImagesResult;
    }
}