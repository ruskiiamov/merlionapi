<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Product;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\ItemsPropertiesSectionsResult;

class GetItemsPropertiesSectionsResponse implements Response
{
    /**
     * @JMS\SerializedName("getItemsPropertiesSectionsResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\ItemsPropertiesSectionsResult>")
     *
     * @var ItemsPropertiesSectionsResult[]
     */
    private $getItemsPropertiesSectionsResult;

    /**
     * @return ItemsPropertiesSectionsResult[]
     */
    public function getItemsPropertiesSectionsResult(): array
    {
        return $this->getItemsPropertiesSectionsResult;
    }
}