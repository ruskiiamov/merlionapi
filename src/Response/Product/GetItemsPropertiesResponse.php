<?php

declare(strict_types=1);

namespace LaptopDev\MerlionApi\Response\Product;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MerlionApi\Contract\Response;
use LaptopDev\MerlionApi\Response\Result\ItemsPropertiesResult;

class GetItemsPropertiesResponse implements Response
{
    /**
     * @JMS\SerializedName("getItemsPropertiesResult")
     * @JMS\XmlList(entry = "item")
     * @JMS\Type("array<LaptopDev\MerlionApi\Response\Result\ItemsPropertiesResult>")
     *
     * @var ItemsPropertiesResult[]
     */
    private $getItemsPropertiesResult;

    /**
     * @return ItemsPropertiesResult[]
     */
    public function getItemsPropertiesResult(): array
    {
        return $this->getItemsPropertiesResult;
    }
}