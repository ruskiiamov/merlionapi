# MerlionApi

## КЛИЕНТ
Необходимо указать адрес сервиса, код клиента, логин и пароль сервиса Merlion
#### Пример:
```php
use LaptopDev\MerlionApi\MerlionApiClient;

$client = new MerlionApiClient(
    'https://api.merlion.com/dl/mlservice3',
    'your_client_code',
    'your_login',
    'your_password'
);
```

## API ПРАЙС-ЛИСТА
### Получение справочника товарных групп
- метод API: getCatalog()
- входные параметры: код товарной группы (также можно тспользовать значения: 
All – элементы всех уровней классификации, Order – список всех направлений)
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Catalog\GetCatalogRequest;

$request = new GetCatalogRequest();
$request->setCatId('your_cat_id');

$response = $client->catalog->sendGetCatalogRequest($request);
```
### Получение справочника товаров
- метод API: getItems()
- входные параметры: код товарной группы, массив с кодами товаров, код метода отгрузки, 
номер страницы, количество записей на странице, метка времени
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Catalog\GetItemsRequest;

$request = new GetItemsRequest();
$request
    ->setCatId('your_cat_id')
    ->addItemId('item1_code')
    ->addItemId('item2_code')
    ->setShipmentMethod('your_shipment_method')
    ->setPage(0)
    ->setRowsOnPage(200)
    ->setLastTimeChange(new DateTime('yesterday'));

$response = $client->catalog->sendGetItemsRequest($request);
```
### Получение доступного количества товаров и цен
- метод API: getItemsAvail()
- входные параметры: код товарной группы, код метода отгрузки, дата отгрузки, 
признак вывода по доступности товаров, массив с кодами товаров
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Catalog\GetItemsAvailRequest;

$request = new GetItemsAvailRequest();
$request
    ->setCatId('your_cat_id')
    ->setShipmentMethod('your_shipment_method')
    ->setShipmentDate('your_shipment_date')
    ->setOnlyAvail('0')
    ->addItemId('item1_code')
    ->addItemId('item2_code');

$response = $client->catalog->sendGetItemsAvailRequest($request);
```
### Получение справочника EAN штрихкодов
- метод API: getItemsBarcodes()
- входные параметры: код товарной группы, массив с кодами товаров
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Catalog\GetItemsBarcodesRequest;

$request = new GetItemsBarcodesRequest();
$request
    ->setCatId('your_cat_id')
    ->addItemId('item1_code')
    ->addItemId('item2_code');

$response = $client->catalog->sendGetItemsBarcodesRequest($request);
```

## API ТОВАРОВ
### Получение характеристик товаров
- метод API: getItemsProperties()
- входные параметры: код товарной группы, массив с кодами товаров, номер страницы, 
количество записей на странице, метка времени
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Product\GetItemsPropertiesRequest;

$request = new GetItemsPropertiesRequest();
$request
    ->setCatId('your_cat_id')
    ->addItemId('item1_code')
    ->addItemId('item2_code')
    ->setPage(10)
    ->setRowsOnPage(500)
    ->setLastTimeChange(new DateTime('2 days ago'));

$response = $client->product->sendGetItemsPropertiesRequest($request);
```
### Получение разделов характеристик
- метод API: getItemsPropertiesSections()
- входные параметры: код раздела характеристики
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Product\GetItemsPropertiesSectionsRequest;

$request = new GetItemsPropertiesSectionsRequest();
$request->setId('your_id');

$response = $client->product->sendGetItemsPropertiesSectionsRequest($request);
```
### Получение изображений товаров
- метод API: getItemsImages()
- входные параметры: код товарной группы, массив с кодами товаров,
  номер страницы, количество записей на странице, метка времени
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Product\GetItemsImagesRequest;

$request = new GetItemsImagesRequest();
$request
    ->setCatId('your_cat_id')
    ->addItemId('item1_code')
    ->addItemId('item2_code')
    ->setPage(20)
    ->setRowsOnPage(1000)
    ->setLastTimeChange(new DateTime('yesterday 14:00'));

$response = $client->product->sendGetItemsImagesRequest($request);
```

## API ЗАКАЗОВ
### Создание/редактирование заголовка заказа
- метод API: setOrderHeaderCommand()
- входные параметры: номер документа, код метода отгрузки, дата отгрузки, код контрагента, 
код агента по доставке, конечный заказчик, комментарий к заказу, представитель, код конечной 
точки доставки, код типа упаковки
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Order\SetOrderHeaderCommandRequest;

$request = new SetOrderHeaderCommandRequest();
$request
    ->setDocumentNo('document_no')
    ->setShipmentMethod('shipment_method')
    ->setShipmentDate('shipment_date')
    ->setCounterAgent('counter_agent')
    ->setShipmentAgent('shipment_agent')
    ->setEndCustomer('end_customer')
    ->setComment('comment')
    ->setRepresentative('representative')
    ->setEndpointDeliveryId(123)
    ->setPackingType('packing_type');

$response = $client->order->sendSetOrderHeaderCommandRequest($request);
```
### Создание/редактирование строки заказа
- метод API: setOrderLineCommand()
- входные параметры: номер документа, в который будет происходить добавление товара, код товара,
желаемое количество товара для резерва, желаемая цена
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Order\SetOrderLineCommandRequest;

$request = new SetOrderLineCommandRequest;
$request
    ->setDocumentNo('document_no')
    ->setItemNo('item_no')
    ->setQty(123)
    ->setPrice(4567.8);

$response = $client->order->sendSetOrderLineCommandRequest($request);
```
### Увеличение/уменьшение жедаемого количества товара в заказе
- метод API: setAddOrderLineCommand()
- входные параметры: номер документа, в котором будет происходить изменение желаемого количества
товара, код товара, величина изменения желаемого количества товара, желаемая цена
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Order\SetAddOrderLineCommandRequest;

$request = new SetAddOrderLineCommandRequest;
$request
    ->setDocumentNo('document_no')
    ->setItemNo('item_no')
    ->setQty(100)
    ->setPrice(2345.6);

$response = $client->order->sendSetAddOrderLineCommandRequest($request);
```
### Перенос резервов в новый заказ
- метод API: setMoveOrderLineCommand()
- входные параметры: номер документа, из которого происходит перенос резервов, код товара,
количество товара для переноса в новый заказ, номер команды
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Order\SetMoveOrderLineCommandRequest;

$request = new SetMoveOrderLineCommandRequest;
$request
    ->setDocumentNo('document_no')
    ->setItemNo('item_no')
    ->setQty(200)
    ->setOperationNo(123);

$response = $client->order->sendSetMoveOrderLineCommandRequest($request);
```
### Подпись заказа
- метод API: setSignOrderCommand()
- входные параметры: номер документа, тип подписи
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Order\SetSignOrderCommandRequest;

$request = new SetSignOrderCommandRequest;
$request
    ->setDocumentNo('document_no')
    ->setSignType(1);

$response = $client->order->sendSetSignOrderCommandRequest($request);
```
### Снятие клиентской подписи с заказа
- метод API: unsetSignOrderCommand()
- входные параметры: номер документа
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Order\UnsetSignOrderCommandRequest;

$request = new UnsetSignOrderCommandRequest;
$request
    ->setDocumentNo('document_no');

$response = $client->order->sendUnsetSignOrderCommandRequest($request);
```
### Удаление заказа
- метод API: setDeleteOrderCommand()
- входные параметры: номер документа
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Order\SetDeleteOrderCommandRequest;

$request = new SetDeleteOrderCommandRequest;
$request
    ->setDocumentNo('document_no');

$response = $client->order->sendSetDeleteOrderCommandRequest($request);
```
### Проверка обработки команд
- метод API: getCommandResult()
- входные параметры: номер команды
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Order\GetCommandResultRequest;

$request = new GetCommandResultRequest;
$request
    ->setOperationNo('operation_no');

$response = $client->order->sendGetCommandResultRequest($request);
```
### Проверка результата обработки по строкам пачки команд
- метод API: getCommandResultDetails()
- входные параметры: номер команды
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Order\GetCommandResultDetailsRequest;

$request = new GetCommandResultDetailsRequest;
$request
    ->setOperationNo('operation_no');

$response = $client->order->sendGetCommandResultDetailsRequest($request);
```
### Получение списка заголовков заказов
- метод API: getOrdersList()
- входные параметры: номер заказа
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Order\GetOrdersListRequest;

$request = new GetOrdersListRequest;
$request
    ->setDocumentNo('document_no');

$response = $client->order->sendGetOrdersListRequest($request);
```
### Получение строк заказа
- метод API: getOrderLines()
- входные параметры: номер заказа, детализация заказа
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Order\GetOrderLinesRequest;

$request = new GetOrderLinesRequest;
$request
    ->setDocumentNo('document_no')
    ->setDetails('details');

$response = $client->order->sendGetOrderLinesRequest($request);
```
### Получение строк заказа с грузовой таможенной декларацией (ГТД)
- метод API: getOrderLinesGTD()
- входные параметры: номер заказа
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Order\GetOrderLinesGtdRequest;

$request = new GetOrderLinesGtdRequest;
$request
    ->setDocumentNo('document_no');

$response = $client->order->sendGetOrderLinesGtdRequest($request);
```

## API ПРИВЯЗКИ ТОВАРОВ
### Добавление/обновление привязок
- метод API: setLinkedItems()
- входные параметры: код товара в компании MERLION, собственный код товара,
собственное наименование товара
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Link\SetLinkedItemsRequest;

$request = new SetLinkedItemsRequest;
$request
    ->setItemId('item_id')
    ->setCustItemId('custitem_id')
    ->setCustItemName('custitem_name');

$response = $client->link->sendSetLinkedItemsRequest($request);
```
### Поиск привязок
- метод API: getLinkedItems()
- входные параметры: номер страницы, количество записей на странице, 
массив с кодами товаров (один или несколько)
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Link\GetLinkedItemsRequest;

$request = new GetLinkedItemsRequest;
$request
    ->setPage(100)
    ->setRowsOnPage(50)
    ->setTypeItemId(0)
    ->addItemId('item_id');

$response = $client->link->sendGetLinkedItemsRequest($request);
```

## API СПРАВОЧНИКОВ
### Получение справочника доступных дат отгрузки
- метод API: getShipmentDates()
- входные параметры: код даты отгрузки, код метода отгрузки
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Manual\GetShipmentDatesRequest;

$request = new GetShipmentDatesRequest;
$request
    ->setCode('code')
    ->setShipmentMethodCode('ShipmentMethodCode');

$response = $client->manual->sendGetShipmentDatesRequest($request);
```
### Получение справочника доступных методов отгрузки
- метод API: getShipmentMethods()
- входные параметры: код метода отгрузки
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Manual\GetShipmentMethodsRequest;

$request = new GetShipmentMethodsRequest;
$request
    ->setCode('code');

$response = $client->manual->sendGetShipmentMethodsRequest($request);
```
### Получение справочника доступных агентов по доставке
- метод API: getShipmentAgents()
- входные параметры: код агента по доставке
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Manual\GetShipmentAgentsRequest;

$request = new GetShipmentAgentsRequest;
$request
    ->setCode('code');

$response = $client->manual->sendGetShipmentAgentsRequest($request);
```
### Получение справочника контрагентов партнера
- метод API: getCounterAgent()
- входные параметры: код контрагента/договора по клиенту партнера
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Manual\GetCounterAgentRequest;

$request = new GetCounterAgentRequest;
$request
    ->setCode('code');

$response = $client->manual->sendGetCounterAgentRequest($request);
```
### Получение справочника представителей по основному договору контрагента
- метод API: getRepresentative()
- входные параметры: код контрагента
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Manual\GetRepresentativeRequest;

$request = new GetRepresentativeRequest;
$request
    ->setCounterAgentCode('CounterAgentCode');

$response = $client->manual->sendGetRepresentativeRequest($request);
```
### Получение справочника конечных точек доставки
- метод API: getEndPointDelivery()
- входные параметры: код конечной точки доставки, код агента по доставке
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Manual\GetEndPointDeliveryRequest;

$request = new GetEndPointDeliveryRequest;
$request
    ->setId(123)
    ->setShippingAgentCode('ShippingAgentCode');

$response = $client->manual->sendGetEndPointDeliveryRequest($request);
```
### Получение справочника типов упаковки
- метод API: getPackingTypes()
- входные параметры: код типа упаковки
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Manual\GetPackingTypesRequest;

$request = new GetPackingTypesRequest;
$request
    ->setCode('code');

$response = $client->manual->sendGetPackingTypesRequest($request);
```
### Получение справочника стран
- метод API: getCountry()
- входные параметры: код страны
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Manual\GetCountryRequest;

$request = new GetCountryRequest;
$request
    ->setCode('code');

$response = $client->manual->sendGetCountryRequest($request);
```
### Получение справочника по курсу валюты
- метод API: getCurrencyRate()
- входные параметры: дата курса валюты
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Manual\GetCurrencyRateRequest;

$request = new GetCurrencyRateRequest;
$request
    ->setDate('date');

$response = $client->manual->sendGetCurrencyRateRequest($request);
```
### Получение справочника товарных сертификатов
- метод API: getCertificates()
- входные параметры: код товара, номер страницы, количество записей на странице
##### Пример выполнения запроса:
```php
use LaptopDev\MerlionApi\Request\Manual\GetCertificatesRequest;

$request = new GetCertificatesRequest;
$request
    ->setItemId(123)
    ->setPage(10)
    ->setRowsOnPage(20);

$response = $client->manual->sendGetCertificatesRequest($request);
```