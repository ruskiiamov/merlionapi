<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Catalog;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Catalog\GetItemsBarcodesResponse;
use LaptopDev\MerlionApi\Response\Result\ItemsBarcodesResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetItemsBarcodesResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getItemsBarcodesResult' => [
                'item#1' => [
                    'ItemNo' => 'test_item_no',
                    'UnitOfMeasure' => 'test_unit_of_measure',
                    'Barcode' => 'test_barcode',
                    'Checked' => 1,
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getItemsBarcodesResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getItemsBarcodesResponse = $this->serializer->deserialize(
            $serialized,
            GetItemsBarcodesResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getItemsBarcodesResponse->getItemsBarcodesResult()
        );

        $this->assertInstanceOf(
            ItemsBarcodesResult::class,
            $getItemsBarcodesResponse->getItemsBarcodesResult()[0]
        );

        $this->assertInstanceOf(
            ItemsBarcodesResult::class,
            $getItemsBarcodesResponse->getItemsBarcodesResult()[1]
        );

        $this->assertEquals(
            $data['getItemsBarcodesResult']['item#1']['ItemNo'],
            $getItemsBarcodesResponse->getItemsBarcodesResult()[0]->itemNo()
        );

        $this->assertEquals(
            $data['getItemsBarcodesResult']['item#1']['UnitOfMeasure'],
            $getItemsBarcodesResponse->getItemsBarcodesResult()[0]->unitOfMeasure()
        );

        $this->assertEquals(
            $data['getItemsBarcodesResult']['item#1']['Barcode'],
            $getItemsBarcodesResponse->getItemsBarcodesResult()[0]->barcode()
        );

        $this->assertEquals(
            $data['getItemsBarcodesResult']['item#1']['Checked'],
            $getItemsBarcodesResponse->getItemsBarcodesResult()[0]->checked()
        );
    }
}