<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Catalog;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Catalog\GetCatalogResponse;
use LaptopDev\MerlionApi\Response\Result\CatalogResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetCatalogResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getCatalogResult' => [
                'item#1' => [
                    'ID' => 'test_id',
                    'ID_PARENT' => 'test_id_parent',
                    'Description' => 'test_description',
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getCatalogResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getCatalogResponse = $this->serializer->deserialize(
            $serialized,
            GetCatalogResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getCatalogResponse->getCatalogResult()
        );

        $this->assertInstanceOf(
            CatalogResult::class,
            $getCatalogResponse->getCatalogResult()[0]
        );

        $this->assertInstanceOf(
            CatalogResult::class,
            $getCatalogResponse->getCatalogResult()[1]
        );

        $this->assertEquals(
            $data['getCatalogResult']['item#1']['ID'],
            $getCatalogResponse->getCatalogResult()[0]->id()
        );

        $this->assertEquals(
            $data['getCatalogResult']['item#1']['ID_PARENT'],
            $getCatalogResponse->getCatalogResult()[0]->idParent()
        );

        $this->assertEquals(
            $data['getCatalogResult']['item#1']['Description'],
            $getCatalogResponse->getCatalogResult()[0]->description()
        );
    }
}