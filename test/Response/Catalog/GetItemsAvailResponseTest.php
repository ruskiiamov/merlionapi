<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Catalog;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Catalog\GetItemsAvailResponse;
use LaptopDev\MerlionApi\Response\Result\ItemsAvailResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetItemsAvailResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getItemsAvailResult' => [
                'item#1' => [
                    'No' => 'test_no',
                    'PriceClient' => 123.4,
                    'PriceClient_RG' => 234.5,
                    'PriceClient_MSK' => 345.6,
                    'AvailableClient' => 123,
                    'AvailableClient_RG' => 234,
                    'AvailableClient_MSK' => 345,
                    'AvailableExpected' => 456,
                    'AvailableExpectedNext' => 567,
                    'DateExpectedNext' => '2021-11-20',
                    'RRP' => 987.6,
                    'RRP_Date' => '2021-12-23',
                    'PriceClientRUB' => 876.5,
                    'PriceClientRUB_RG' => 765.4,
                    'PriceClientRUB_MSK' => 654.3,
                    'Online_Reserve' => 0,
                    'ReserveCost' => 0.00123,
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getItemsAvailResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getItemsAvailResponse = $this->serializer->deserialize(
            $serialized,
            GetItemsAvailResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getItemsAvailResponse->getItemsAvailResult()
        );

        $this->assertInstanceOf(
            ItemsAvailResult::class,
            $getItemsAvailResponse->getItemsAvailResult()[0]
        );

        $this->assertInstanceOf(
            ItemsAvailResult::class,
            $getItemsAvailResponse->getItemsAvailResult()[1]
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['No'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->no()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['PriceClient'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->priceClient()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['PriceClient_RG'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->priceClientRg()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['PriceClient_MSK'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->priceClientMsk()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['AvailableClient'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->availableClient()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['AvailableClient_RG'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->availableClientRg()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['AvailableClient_MSK'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->availableClientMsk()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['AvailableExpected'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->availableExpected()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['AvailableExpectedNext'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->availableExpectedNext()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['DateExpectedNext'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->dateExpectedNext()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['RRP'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->rrp()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['RRP_Date'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->rrpDate()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['PriceClientRUB'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->priceClientRub()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['PriceClientRUB_RG'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->priceClientRubRg()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['PriceClientRUB_MSK'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->priceClientRubMsk()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['Online_Reserve'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->onlineReserve()
        );

        $this->assertEquals(
            $data['getItemsAvailResult']['item#1']['ReserveCost'],
            $getItemsAvailResponse->getItemsAvailResult()[0]->reserveCost()
        );
    }
}