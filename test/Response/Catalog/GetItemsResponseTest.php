<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Catalog;

use DateTime;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Catalog\GetItemsResponse;
use LaptopDev\MerlionApi\Response\Result\ItemsResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetItemsResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getItemsResult' => [
                'item#1' => [
                    'No' => 'test_no',
                    'Name' => 'test_name',
                    'Brand' => 'test_brand',
                    'Vendor_part' => 'test_vendor_part',
                    'Size' => '',
                    'EOL' => 1,
                    'Warranty' => 24,
                    'Weight' => 1.34,
                    'Volume' => 0.34,
                    'Min_Packaged' => 2,
                    'Sales_Limit_Type' => 'Кратно',
                    'GroupName1' => 'test_group_name1',
                    'GroupName2' => 'test_group_name2',
                    'GroupName3' => 'test_group_name3',
                    'GroupCode1' => 'test_group_code1',
                    'GroupCode2' => 'test_group_code2',
                    'GroupCode3' => 'test_group_code3',
                    'IsBundle' => 0,
                    'ActionDesc' => 'test_action_desc',
                    'ActionWWW' => 'test_action_www',
                    'Last_time_modified' => '2021-11-20T18:50:30',
                    'VAT' => '20%',
                    'IsNew' => 1,
                    'Length' => 0.98,
                    'Width' => 0.43,
                    'Height' => 0.81,
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getItemsResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getItemsResponse = $this->serializer->deserialize(
            $serialized,
            GetItemsResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getItemsResponse->getItemsResult()
        );

        $this->assertInstanceOf(
            ItemsResult::class,
            $getItemsResponse->getItemsResult()[0]
        );

        $this->assertInstanceOf(
            ItemsResult::class,
            $getItemsResponse->getItemsResult()[1]
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['No'],
            $getItemsResponse->getItemsResult()[0]->no()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['Name'],
            $getItemsResponse->getItemsResult()[0]->name()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['Brand'],
            $getItemsResponse->getItemsResult()[0]->brand()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['Vendor_part'],
            $getItemsResponse->getItemsResult()[0]->vendorPart()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['EOL'],
            $getItemsResponse->getItemsResult()[0]->eol()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['Warranty'],
            $getItemsResponse->getItemsResult()[0]->warranty()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['Weight'],
            $getItemsResponse->getItemsResult()[0]->weight()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['Volume'],
            $getItemsResponse->getItemsResult()[0]->volume()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['Min_Packaged'],
            $getItemsResponse->getItemsResult()[0]->minPackaged()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['Sales_Limit_Type'],
            $getItemsResponse->getItemsResult()[0]->salesLimitType()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['GroupName1'],
            $getItemsResponse->getItemsResult()[0]->groupName1()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['GroupName2'],
            $getItemsResponse->getItemsResult()[0]->groupName2()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['GroupName3'],
            $getItemsResponse->getItemsResult()[0]->groupName3()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['GroupCode1'],
            $getItemsResponse->getItemsResult()[0]->groupCode1()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['GroupCode2'],
            $getItemsResponse->getItemsResult()[0]->groupCode2()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['GroupCode3'],
            $getItemsResponse->getItemsResult()[0]->groupCode3()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['IsBundle'],
            $getItemsResponse->getItemsResult()[0]->isBundle()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['ActionDesc'],
            $getItemsResponse->getItemsResult()[0]->actionDesc()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['ActionWWW'],
            $getItemsResponse->getItemsResult()[0]->actionWww()
        );

        $this->assertEquals(
            new DateTime($data['getItemsResult']['item#1']['Last_time_modified']),
            $getItemsResponse->getItemsResult()[0]->lastTimeModified()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['VAT'],
            $getItemsResponse->getItemsResult()[0]->vat()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['IsNew'],
            $getItemsResponse->getItemsResult()[0]->isNew()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['Length'],
            $getItemsResponse->getItemsResult()[0]->length()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['Width'],
            $getItemsResponse->getItemsResult()[0]->width()
        );

        $this->assertEquals(
            $data['getItemsResult']['item#1']['Height'],
            $getItemsResponse->getItemsResult()[0]->height()
        );
    }
}