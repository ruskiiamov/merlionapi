<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Product;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Product\GetItemsPropertiesSectionsResponse;
use LaptopDev\MerlionApi\Response\Result\ItemsPropertiesSectionsResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetItemsPropertiesSectionsResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getItemsPropertiesSectionsResult' => [
                'item#1' => [
                    'id' => 123,
                    'name' => 'test_name',
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getItemsPropertiesSectionsResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getItemsPropertiesSectionsResponse = $this->serializer->deserialize(
            $serialized,
            GetItemsPropertiesSectionsResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getItemsPropertiesSectionsResponse->getItemsPropertiesSectionsResult()
        );

        $this->assertInstanceOf(
            ItemsPropertiesSectionsResult::class,
            $getItemsPropertiesSectionsResponse->getItemsPropertiesSectionsResult()[0]
        );

        $this->assertInstanceOf(
            ItemsPropertiesSectionsResult::class,
            $getItemsPropertiesSectionsResponse->getItemsPropertiesSectionsResult()[1]
        );

        $this->assertEquals(
            $data['getItemsPropertiesSectionsResult']['item#1']['id'],
            $getItemsPropertiesSectionsResponse->getItemsPropertiesSectionsResult()[0]->id()
        );

        $this->assertEquals(
            $data['getItemsPropertiesSectionsResult']['item#1']['name'],
            $getItemsPropertiesSectionsResponse->getItemsPropertiesSectionsResult()[0]->name()
        );
    }
}