<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Product;

use DateTime;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Product\GetItemsPropertiesResponse;
use LaptopDev\MerlionApi\Response\Result\ItemsPropertiesResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetItemsPropertiesResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getItemsPropertiesResult' => [
                'item#1' => [
                    'No' => 'test_no',
                    'PropertyID' => 123,
                    'PropertyName' => 'test_property_name',
                    'Sorting' => 1,
                    'Value' => 'test_value',
                    'Measure_Name' => 'test_measure_name',
                    'Last_time_modified' => '2021-11-20T22:28:22',
                    'Section_Id' => 'test_section_id',
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getItemsPropertiesResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getItemsPropertiesResponse = $this->serializer->deserialize(
            $serialized,
            GetItemsPropertiesResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getItemsPropertiesResponse->getItemsPropertiesResult()
        );

        $this->assertInstanceOf(
            ItemsPropertiesResult::class,
            $getItemsPropertiesResponse->getItemsPropertiesResult()[0]
        );

        $this->assertInstanceOf(
            ItemsPropertiesResult::class,
            $getItemsPropertiesResponse->getItemsPropertiesResult()[1]
        );

        $this->assertEquals(
            $data['getItemsPropertiesResult']['item#1']['No'],
            $getItemsPropertiesResponse->getItemsPropertiesResult()[0]->no()
        );

        $this->assertEquals(
            $data['getItemsPropertiesResult']['item#1']['PropertyID'],
            $getItemsPropertiesResponse->getItemsPropertiesResult()[0]->propertyId()
        );

        $this->assertEquals(
            $data['getItemsPropertiesResult']['item#1']['PropertyName'],
            $getItemsPropertiesResponse->getItemsPropertiesResult()[0]->propertyName()
        );

        $this->assertEquals(
            $data['getItemsPropertiesResult']['item#1']['Sorting'],
            $getItemsPropertiesResponse->getItemsPropertiesResult()[0]->sorting()
        );

        $this->assertEquals(
            $data['getItemsPropertiesResult']['item#1']['Value'],
            $getItemsPropertiesResponse->getItemsPropertiesResult()[0]->value()
        );

        $this->assertEquals(
            $data['getItemsPropertiesResult']['item#1']['Measure_Name'],
            $getItemsPropertiesResponse->getItemsPropertiesResult()[0]->measureName()
        );

        $this->assertEquals(
            new DateTime($data['getItemsPropertiesResult']['item#1']['Last_time_modified']),
            $getItemsPropertiesResponse->getItemsPropertiesResult()[0]->lastTimeModified()
        );

        $this->assertEquals(
            $data['getItemsPropertiesResult']['item#1']['Section_Id'],
            $getItemsPropertiesResponse->getItemsPropertiesResult()[0]->sectionId()
        );
    }
}