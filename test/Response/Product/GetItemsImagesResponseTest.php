<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Product;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Product\GetItemsImagesResponse;
use LaptopDev\MerlionApi\Response\Result\ItemsImagesResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetItemsImagesResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getItemsImagesResult' => [
                'item#1' => [
                    'No' => 'test_no',
                    'ViewType' => 'p',
                    'SizeType' => 'b',
                    'FileName' => 'image.jpeg',
                    'Created' => '2021-11-20T21:36:33',
                    'Size' => 256,
                    'Width' => 800,
                    'Height' => 600,
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getItemsImagesResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getItemsImagesResponse = $this->serializer->deserialize(
            $serialized,
            GetItemsImagesResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getItemsImagesResponse->getItemsImagesResult()
        );

        $this->assertInstanceOf(
            ItemsImagesResult::class,
            $getItemsImagesResponse->getItemsImagesResult()[0]
        );

        $this->assertInstanceOf(
            ItemsImagesResult::class,
            $getItemsImagesResponse->getItemsImagesResult()[1]
        );

        $this->assertEquals(
            $data['getItemsImagesResult']['item#1']['No'],
            $getItemsImagesResponse->getItemsImagesResult()[0]->no()
        );

        $this->assertEquals(
            $data['getItemsImagesResult']['item#1']['ViewType'],
            $getItemsImagesResponse->getItemsImagesResult()[0]->viewType()
        );

        $this->assertEquals(
            $data['getItemsImagesResult']['item#1']['SizeType'],
            $getItemsImagesResponse->getItemsImagesResult()[0]->sizeType()
        );

        $this->assertEquals(
            $data['getItemsImagesResult']['item#1']['FileName'],
            $getItemsImagesResponse->getItemsImagesResult()[0]->fileName()
        );

        $this->assertEquals(
            $data['getItemsImagesResult']['item#1']['Created'],
            $getItemsImagesResponse->getItemsImagesResult()[0]->created()
        );

        $this->assertEquals(
            $data['getItemsImagesResult']['item#1']['Size'],
            $getItemsImagesResponse->getItemsImagesResult()[0]->size()
        );

        $this->assertEquals(
            $data['getItemsImagesResult']['item#1']['Width'],
            $getItemsImagesResponse->getItemsImagesResult()[0]->width()
        );

        $this->assertEquals(
            $data['getItemsImagesResult']['item#1']['Height'],
            $getItemsImagesResponse->getItemsImagesResult()[0]->height()
        );
    }
}