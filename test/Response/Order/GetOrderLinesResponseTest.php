<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Order\GetOrderLinesResponse;
use LaptopDev\MerlionApi\Response\Result\OrderLinesResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetOrderLinesResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getOrderLinesResult' => [
                'item#1' => [
                    'item_no' => 'test_item_no',
                    'document_no' => 'test_document_no',
                    'qty' => 100,
                    'desire_qty' => 200,
                    'shipped_qty' => 50,
                    'price' => 1234.5,
                    'amount' => 15456,
                    'desire_price' => 1000.5,
                    'weight' => 256.3,
                    'volume' => 2.5,
                    'ReserveTime' => 2.25,
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getOrderLinesResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getOrderLinesResponse = $this->serializer->deserialize(
            $serialized,
            GetOrderLinesResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getOrderLinesResponse->getOrderLinesResult()
        );

        $this->assertInstanceOf(
            OrderLinesResult::class,
            $getOrderLinesResponse->getOrderLinesResult()[0]
        );

        $this->assertInstanceOf(
            OrderLinesResult::class,
            $getOrderLinesResponse->getOrderLinesResult()[1]
        );

        $this->assertEquals(
            $data['getOrderLinesResult']['item#1']['item_no'],
            $getOrderLinesResponse->getOrderLinesResult()[0]->itemNo()
        );

        $this->assertEquals(
            $data['getOrderLinesResult']['item#1']['document_no'],
            $getOrderLinesResponse->getOrderLinesResult()[0]->documentNo()
        );

        $this->assertEquals(
            $data['getOrderLinesResult']['item#1']['qty'],
            $getOrderLinesResponse->getOrderLinesResult()[0]->qty()
        );

        $this->assertEquals(
            $data['getOrderLinesResult']['item#1']['desire_qty'],
            $getOrderLinesResponse->getOrderLinesResult()[0]->desireQty()
        );

        $this->assertEquals(
            $data['getOrderLinesResult']['item#1']['shipped_qty'],
            $getOrderLinesResponse->getOrderLinesResult()[0]->shippedQty()
        );

        $this->assertEquals(
            $data['getOrderLinesResult']['item#1']['shipped_qty'],
            $getOrderLinesResponse->getOrderLinesResult()[0]->shippedQty()
        );

        $this->assertEquals(
            $data['getOrderLinesResult']['item#1']['price'],
            $getOrderLinesResponse->getOrderLinesResult()[0]->price()
        );

        $this->assertEquals(
            $data['getOrderLinesResult']['item#1']['amount'],
            $getOrderLinesResponse->getOrderLinesResult()[0]->amount()
        );

        $this->assertEquals(
            $data['getOrderLinesResult']['item#1']['desire_price'],
            $getOrderLinesResponse->getOrderLinesResult()[0]->desirePrice()
        );

        $this->assertEquals(
            $data['getOrderLinesResult']['item#1']['weight'],
            $getOrderLinesResponse->getOrderLinesResult()[0]->weight()
        );

        $this->assertEquals(
            $data['getOrderLinesResult']['item#1']['volume'],
            $getOrderLinesResponse->getOrderLinesResult()[0]->volume()
        );

        $this->assertEquals(
            $data['getOrderLinesResult']['item#1']['ReserveTime'],
            $getOrderLinesResponse->getOrderLinesResult()[0]->reserveTime()
        );
    }
}