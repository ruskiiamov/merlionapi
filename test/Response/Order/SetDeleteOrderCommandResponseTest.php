<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Order\SetDeleteOrderCommandResponse;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class SetDeleteOrderCommandResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'setDeleteOrderCommandResult' => 123,
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'setDeleteOrderCommandResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $setDeleteOrderCommandResponse = $this->serializer->deserialize(
            $serialized,
            SetDeleteOrderCommandResponse::class,
            'soap'
        );


        $this->assertEquals(
            $data['setDeleteOrderCommandResult'],
            $setDeleteOrderCommandResponse->setDeleteOrderCommandResult()
        );
    }
}