<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Order\SetSignOrderCommandResponse;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class SetSignOrderCommandResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'setSignOrderCommandResult' => 123,
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'setSignOrderCommandResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $setSignOrderCommandResponse = $this->serializer->deserialize(
            $serialized,
            SetSignOrderCommandResponse::class,
            'soap'
        );


        $this->assertEquals(
            $data['setSignOrderCommandResult'],
            $setSignOrderCommandResponse->setSignOrderCommandResult()
        );
    }
}