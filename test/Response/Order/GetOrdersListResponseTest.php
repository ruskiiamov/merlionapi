<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Order\GetOrdersListResponse;
use LaptopDev\MerlionApi\Response\Result\OrdersListResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetOrdersListResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getOrdersListResult' => [
                'item#1' => [
                    'document_no' => 'test_document_no',
                    'PostedDocumentNo' => 'test_PostedDocumentNo',
                    'TNN' => 'test_TNN',
                    'OrderDate' => 'test_OrderDate',
                    'Manager' => 'test_Manager',
                    'Contact' => 'test_Contact',
                    'ShipmentMethod' => 'test_ShipmentMethod',
                    'ShipmentMethodCode' => 'test_ShipmentMethodCode',
                    'ShipmentDate' => 'test_ShipmentDate',
                    'ActualShipmentDate' => 'test_ActualShipmentDate',
                    'CounterpartyClient' => 'test_CounterpartyClient',
                    'CounterpartyClientCode' => 'test_CounterpartyClientCode',
                    'ShippingAgent' => 'test_ShippingAgent',
                    'ShippingAgentCode' => 'test_ShippingAgentCode',
                    'EndCustomer' => 'test_EndCustomer',
                    'PostingDescription' => 'test_PostingDescription',
                    'Weight' => 'test_Weight',
                    'Volume' => 1.2,
                    'Amount' => 1234.5,
                    'AmountRUR' => 87654.3,
                    'WillDeleteTomorrow' => 'test_WillDeleteTomorrow',
                    'Status' => 'test_Status',
                    'EndPointCity' => 'test_EndPointCity',
                    'EndPointAdress' => 'test_EndPointAddress',
                    'EndPointContact' => 'test_EndPointContact',
                    'PackingType' => 'test_PackingType',
                    'Representative' => 'test_Representative',
                    'InvoiceNo' => 'test_InvoiceNo',
                    'ContractId' => 'test_ContractId',
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getOrdersListResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getOrdersListResponse = $this->serializer->deserialize(
            $serialized,
            GetOrdersListResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getOrdersListResponse->getOrdersListResult()
        );

        $this->assertInstanceOf(
            OrdersListResult::class,
            $getOrdersListResponse->getOrdersListResult()[0]
        );

        $this->assertInstanceOf(
            OrdersListResult::class,
            $getOrdersListResponse->getOrdersListResult()[1]
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['document_no'],
            $getOrdersListResponse->getOrdersListResult()[0]->documentNo()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['PostedDocumentNo'],
            $getOrdersListResponse->getOrdersListResult()[0]->postedDocumentNo()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['TNN'],
            $getOrdersListResponse->getOrdersListResult()[0]->tnn()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['OrderDate'],
            $getOrdersListResponse->getOrdersListResult()[0]->orderDate()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['Manager'],
            $getOrdersListResponse->getOrdersListResult()[0]->manager()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['Contact'],
            $getOrdersListResponse->getOrdersListResult()[0]->contact()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['ShipmentMethod'],
            $getOrdersListResponse->getOrdersListResult()[0]->shipmentMethod()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['ShipmentMethodCode'],
            $getOrdersListResponse->getOrdersListResult()[0]->shipmentMethodCode()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['ShipmentDate'],
            $getOrdersListResponse->getOrdersListResult()[0]->shipmentDate()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['ActualShipmentDate'],
            $getOrdersListResponse->getOrdersListResult()[0]->actualShipmentDate()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['CounterpartyClient'],
            $getOrdersListResponse->getOrdersListResult()[0]->counterpartyClient()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['CounterpartyClientCode'],
            $getOrdersListResponse->getOrdersListResult()[0]->counterpartyClientCode()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['ShippingAgent'],
            $getOrdersListResponse->getOrdersListResult()[0]->shippingAgent()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['ShippingAgentCode'],
            $getOrdersListResponse->getOrdersListResult()[0]->shippingAgentCode()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['EndCustomer'],
            $getOrdersListResponse->getOrdersListResult()[0]->endCustomer()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['PostingDescription'],
            $getOrdersListResponse->getOrdersListResult()[0]->postingDescription()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['Weight'],
            $getOrdersListResponse->getOrdersListResult()[0]->weight()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['Volume'],
            $getOrdersListResponse->getOrdersListResult()[0]->volume()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['Amount'],
            $getOrdersListResponse->getOrdersListResult()[0]->amount()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['AmountRUR'],
            $getOrdersListResponse->getOrdersListResult()[0]->amountRur()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['WillDeleteTomorrow'],
            $getOrdersListResponse->getOrdersListResult()[0]->willDeleteTomorrow()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['Status'],
            $getOrdersListResponse->getOrdersListResult()[0]->status()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['EndPointCity'],
            $getOrdersListResponse->getOrdersListResult()[0]->endPointCity()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['EndPointAdress'],
            $getOrdersListResponse->getOrdersListResult()[0]->endPointAdress()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['EndPointContact'],
            $getOrdersListResponse->getOrdersListResult()[0]->endPointContact()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['PackingType'],
            $getOrdersListResponse->getOrdersListResult()[0]->packingType()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['Representative'],
            $getOrdersListResponse->getOrdersListResult()[0]->representative()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['InvoiceNo'],
            $getOrdersListResponse->getOrdersListResult()[0]->invoiceNo()
        );

        $this->assertEquals(
            $data['getOrdersListResult']['item#1']['ContractId'],
            $getOrdersListResponse->getOrdersListResult()[0]->contractId()
        );
    }
}