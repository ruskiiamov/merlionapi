<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Order\UnsetSignOrderCommandResponse;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class UnsetSignOrderCommandResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'unsetSignOrderCommandResult' => 123,
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'unsetSignOrderCommandResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $unsetSignOrderCommandResponse = $this->serializer->deserialize(
            $serialized,
            UnsetSignOrderCommandResponse::class,
            'soap'
        );


        $this->assertEquals(
            $data['unsetSignOrderCommandResult'],
            $unsetSignOrderCommandResponse->unsetSignOrderCommandResult()
        );
    }
}