<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Order\SetOrderLineCommandResponse;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class SetOrderLineCommandResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'setOrderLineCommandResult' => 123,
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'setOrderLineCommandResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $setOrderLineCommandResponse = $this->serializer->deserialize(
            $serialized,
            SetOrderLineCommandResponse::class,
            'soap'
        );


        $this->assertEquals(
            $data['setOrderLineCommandResult'],
            $setOrderLineCommandResponse->setOrderLineCommandResult()
        );
    }
}