<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Order\SetAddOrderLineCommandResponse;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class SetAddOrderLineCommandResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'setAddOrderLineCommandResult' => 123,
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'setAddOrderLineCommandResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $setAddOrderLineCommandResponse = $this->serializer->deserialize(
            $serialized,
            SetAddOrderLineCommandResponse::class,
            'soap'
        );


        $this->assertEquals(
            $data['setAddOrderLineCommandResult'],
            $setAddOrderLineCommandResponse->setAddOrderLineCommandResult()
        );
    }
}