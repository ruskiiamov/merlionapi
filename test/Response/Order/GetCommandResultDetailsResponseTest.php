<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Order\GetCommandResultDetailsResponse;
use LaptopDev\MerlionApi\Response\Result\CommandResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetCommandResultDetailsResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getCommandResultDetailsResult' => [
                'item#1' => [
                    'operation_no' => 123,
                    'item_no' => 'test_item_no',
                    'CreateTime' => 'test_CreateTime',
                    'ProcessingTime' => 'test_ProcessingTime',
                    'EndingTime' => 'test_EndingTime',
                    'ProcessingResult' => 'test_ProcessingResult',
                    'DocumentNo' => 'test_DocumentNo',
                    'DocumentNo2' => 'test_DocumentNo2',
                    'ProcessingResultComment' => 'test_ProcessingResultComment',
                    'ErrorText' => 'test_ErrorText',
                    'ProcessingReserved' => 'test_ProcessingReserved',
                    'OperationLineNo' => 321,
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getCommandResultDetailsResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getCommandResultDetailsResponse = $this->serializer->deserialize(
            $serialized,
            GetCommandResultDetailsResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()
        );

        $this->assertInstanceOf(
            CommandResult::class,
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()[0]
        );

        $this->assertInstanceOf(
            CommandResult::class,
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()[1]
        );

        $this->assertEquals(
            $data['getCommandResultDetailsResult']['item#1']['operation_no'],
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()[0]->operationNo()
        );

        $this->assertEquals(
            $data['getCommandResultDetailsResult']['item#1']['item_no'],
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()[0]->itemNo()
        );

        $this->assertEquals(
            $data['getCommandResultDetailsResult']['item#1']['CreateTime'],
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()[0]->createTime()
        );

        $this->assertEquals(
            $data['getCommandResultDetailsResult']['item#1']['ProcessingTime'],
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()[0]->processingTime()
        );

        $this->assertEquals(
            $data['getCommandResultDetailsResult']['item#1']['EndingTime'],
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()[0]->endingTime()
        );

        $this->assertEquals(
            $data['getCommandResultDetailsResult']['item#1']['ProcessingResult'],
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()[0]->processingResult()
        );

        $this->assertEquals(
            $data['getCommandResultDetailsResult']['item#1']['DocumentNo'],
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()[0]->documentNo()
        );

        $this->assertEquals(
            $data['getCommandResultDetailsResult']['item#1']['DocumentNo2'],
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()[0]->documentNo2()
        );

        $this->assertEquals(
            $data['getCommandResultDetailsResult']['item#1']['ProcessingResultComment'],
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()[0]->processingResultComment()
        );

        $this->assertEquals(
            $data['getCommandResultDetailsResult']['item#1']['ErrorText'],
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()[0]->errorText()
        );

        $this->assertEquals(
            $data['getCommandResultDetailsResult']['item#1']['ProcessingReserved'],
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()[0]->processingReserved()
        );

        $this->assertEquals(
            $data['getCommandResultDetailsResult']['item#1']['OperationLineNo'],
            $getCommandResultDetailsResponse->getCommandResultDetailsResult()[0]->operationLineNo()
        );
    }
}