<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Order\GetOrderLinesGtdResponse;
use LaptopDev\MerlionApi\Response\Result\OrderLinesGtdResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetOrderLinesGtdResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getOrderLinesGTDResult' => [
                'item#1' => [
                    'document_no' => 'test_document_no',
                    'item_no' => 'test_item_no',
                    'bundle_item_no' => 'test_bundle_item_no',
                    'gtd_no' => 'test_gtd_no',
                    'used_qty' => 12,
                    'country' => 'test_country',
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getOrderLinesGtdResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getOrderLinesGtdResponse = $this->serializer->deserialize(
            $serialized,
            GetOrderLinesGtdResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getOrderLinesGtdResponse->getOrderLinesGtdResult()
        );

        $this->assertInstanceOf(
            OrderLinesGtdResult::class,
            $getOrderLinesGtdResponse->getOrderLinesGtdResult()[0]
        );

        $this->assertInstanceOf(
            OrderLinesGtdResult::class,
            $getOrderLinesGtdResponse->getOrderLinesGtdResult()[1]
        );

        $this->assertEquals(
            $data['getOrderLinesGTDResult']['item#1']['document_no'],
            $getOrderLinesGtdResponse->getOrderLinesGtdResult()[0]->documentNo()
        );

        $this->assertEquals(
            $data['getOrderLinesGTDResult']['item#1']['item_no'],
            $getOrderLinesGtdResponse->getOrderLinesGtdResult()[0]->itemNo()
        );

        $this->assertEquals(
            $data['getOrderLinesGTDResult']['item#1']['bundle_item_no'],
            $getOrderLinesGtdResponse->getOrderLinesGtdResult()[0]->bundleItemNo()
        );

        $this->assertEquals(
            $data['getOrderLinesGTDResult']['item#1']['gtd_no'],
            $getOrderLinesGtdResponse->getOrderLinesGtdResult()[0]->gtdNo()
        );

        $this->assertEquals(
            $data['getOrderLinesGTDResult']['item#1']['used_qty'],
            $getOrderLinesGtdResponse->getOrderLinesGtdResult()[0]->usedQty()
        );

        $this->assertEquals(
            $data['getOrderLinesGTDResult']['item#1']['country'],
            $getOrderLinesGtdResponse->getOrderLinesGtdResult()[0]->country()
        );
    }
}