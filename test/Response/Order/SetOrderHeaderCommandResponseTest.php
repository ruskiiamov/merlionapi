<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Order\SetOrderHeaderCommandResponse;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class SetOrderHeaderCommandResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'setOrderHeaderCommandResult' => 123,
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'setOrderHeaderCommandResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $setOrderHeaderCommandResponse = $this->serializer->deserialize(
            $serialized,
            SetOrderHeaderCommandResponse::class,
            'soap'
        );


        $this->assertEquals(
            $data['setOrderHeaderCommandResult'],
            $setOrderHeaderCommandResponse->setOrderHeaderCommandResult()
        );
    }
}