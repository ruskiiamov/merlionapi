<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Order\SetMoveOrderLineCommandResponse;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class SetMoveOrderLineCommandResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'setMoveOrderLineCommandResult' => 123,
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'setMoveOrderLineCommandResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $setMoveOrderLineCommandResponse = $this->serializer->deserialize(
            $serialized,
            SetMoveOrderLineCommandResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['setMoveOrderLineCommandResult'],
            $setMoveOrderLineCommandResponse->setMoveOrderLineCommandResult()
        );
    }
}