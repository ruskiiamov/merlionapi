<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Order\GetCommandResultResponse;
use LaptopDev\MerlionApi\Response\Result\CommandResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetCommandResultResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getCommandResultResult' => [
                'item#1' => [
                    'operation_no' => 123,
                    'item_no' => 'test_item_no',
                    'CreateTime' => 'test_CreateTime',
                    'ProcessingTime' => 'test_ProcessingTime',
                    'EndingTime' => 'test_EndingTime',
                    'ProcessingResult' => 'test_ProcessingResult',
                    'DocumentNo' => 'test_DocumentNo',
                    'DocumentNo2' => 'test_DocumentNo2',
                    'ProcessingResultComment' => 'test_ProcessingResultComment',
                    'ErrorText' => 'test_ErrorText',
                    'ProcessingReserved' => 'test_ProcessingReserved',
                    'OperationLineNo' => 321,
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getCommandResultResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getCommandResultResponse = $this->serializer->deserialize(
            $serialized,
            GetCommandResultResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getCommandResultResponse->getCommandResultResult()
        );

        $this->assertInstanceOf(
            CommandResult::class,
            $getCommandResultResponse->getCommandResultResult()[0]
        );

        $this->assertInstanceOf(
            CommandResult::class,
            $getCommandResultResponse->getCommandResultResult()[1]
        );

        $this->assertEquals(
            $data['getCommandResultResult']['item#1']['operation_no'],
            $getCommandResultResponse->getCommandResultResult()[0]->operationNo()
        );

        $this->assertEquals(
            $data['getCommandResultResult']['item#1']['item_no'],
            $getCommandResultResponse->getCommandResultResult()[0]->itemNo()
        );

        $this->assertEquals(
            $data['getCommandResultResult']['item#1']['CreateTime'],
            $getCommandResultResponse->getCommandResultResult()[0]->createTime()
        );

        $this->assertEquals(
            $data['getCommandResultResult']['item#1']['ProcessingTime'],
            $getCommandResultResponse->getCommandResultResult()[0]->processingTime()
        );

        $this->assertEquals(
            $data['getCommandResultResult']['item#1']['EndingTime'],
            $getCommandResultResponse->getCommandResultResult()[0]->endingTime()
        );

        $this->assertEquals(
            $data['getCommandResultResult']['item#1']['ProcessingResult'],
            $getCommandResultResponse->getCommandResultResult()[0]->processingResult()
        );

        $this->assertEquals(
            $data['getCommandResultResult']['item#1']['DocumentNo'],
            $getCommandResultResponse->getCommandResultResult()[0]->documentNo()
        );

        $this->assertEquals(
            $data['getCommandResultResult']['item#1']['DocumentNo2'],
            $getCommandResultResponse->getCommandResultResult()[0]->documentNo2()
        );

        $this->assertEquals(
            $data['getCommandResultResult']['item#1']['ProcessingResultComment'],
            $getCommandResultResponse->getCommandResultResult()[0]->processingResultComment()
        );

        $this->assertEquals(
            $data['getCommandResultResult']['item#1']['ErrorText'],
            $getCommandResultResponse->getCommandResultResult()[0]->errorText()
        );

        $this->assertEquals(
            $data['getCommandResultResult']['item#1']['ProcessingReserved'],
            $getCommandResultResponse->getCommandResultResult()[0]->processingReserved()
        );

        $this->assertEquals(
            $data['getCommandResultResult']['item#1']['OperationLineNo'],
            $getCommandResultResponse->getCommandResultResult()[0]->operationLineNo()
        );
    }
}