<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Manual;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Manual\GetCountryResponse;
use LaptopDev\MerlionApi\Response\Result\DictionaryResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetCountryResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getCountryResult' => [
                'item#1' => [
                    'Code' => 'test_Code',
                    'Description' => 'test_Description',
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getCountryResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getCountryResponse = $this->serializer->deserialize(
            $serialized,
            GetCountryResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getCountryResponse->getCountryResult()
        );

        $this->assertInstanceOf(
            DictionaryResult::class,
            $getCountryResponse->getCountryResult()[0]
        );

        $this->assertInstanceOf(
            DictionaryResult::class,
            $getCountryResponse->getCountryResult()[1]
        );

        $this->assertEquals(
            $data['getCountryResult']['item#1']['Code'],
            $getCountryResponse->getCountryResult()[0]->code()
        );

        $this->assertEquals(
            $data['getCountryResult']['item#1']['Description'],
            $getCountryResponse->getCountryResult()[0]->description()
        );
    }
}