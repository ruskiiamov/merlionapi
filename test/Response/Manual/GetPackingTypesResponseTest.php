<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Manual;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Manual\GetPackingTypesResponse;
use LaptopDev\MerlionApi\Response\Result\DictionaryResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetPackingTypesResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getPackingTypesResult' => [
                'item#1' => [
                    'Code' => 'test_Code',
                    'Description' => 'test_Description',
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getPackingTypesResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getPackingTypesResponse = $this->serializer->deserialize(
            $serialized,
            GetPackingTypesResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getPackingTypesResponse->getPackingTypesResult()
        );

        $this->assertInstanceOf(
            DictionaryResult::class,
            $getPackingTypesResponse->getPackingTypesResult()[0]
        );

        $this->assertInstanceOf(
            DictionaryResult::class,
            $getPackingTypesResponse->getPackingTypesResult()[1]
        );

        $this->assertEquals(
            $data['getPackingTypesResult']['item#1']['Code'],
            $getPackingTypesResponse->getPackingTypesResult()[0]->code()
        );

        $this->assertEquals(
            $data['getPackingTypesResult']['item#1']['Description'],
            $getPackingTypesResponse->getPackingTypesResult()[0]->description()
        );
    }
}