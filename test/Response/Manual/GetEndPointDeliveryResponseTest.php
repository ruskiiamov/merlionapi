<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Manual;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Manual\GetEndPointDeliveryResponse;
use LaptopDev\MerlionApi\Response\Result\EndPointDeliveryResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetEndPointDeliveryResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getEndPointDeliveryResult' => [
                'item#1' => [
                    'ID' => 123,
                    'Endpoint_address' => 'test_Endpoint_address',
                    'Endpoint_contact' => 'test_Endpoint_contact',
                    'ShippingAgentCode' => 'test_ShippingAgentCode',
                    'City' => 'test_City',
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getEndPointDeliveryResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getEndPointDeliveryResponse = $this->serializer->deserialize(
            $serialized,
            GetEndPointDeliveryResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getEndPointDeliveryResponse->getEndPointDeliveryResult()
        );

        $this->assertInstanceOf(
            EndPointDeliveryResult::class,
            $getEndPointDeliveryResponse->getEndPointDeliveryResult()[0]
        );

        $this->assertInstanceOf(
            EndPointDeliveryResult::class,
            $getEndPointDeliveryResponse->getEndPointDeliveryResult()[1]
        );

        $this->assertEquals(
            $data['getEndPointDeliveryResult']['item#1']['ID'],
            $getEndPointDeliveryResponse->getEndPointDeliveryResult()[0]->id()
        );

        $this->assertEquals(
            $data['getEndPointDeliveryResult']['item#1']['Endpoint_address'],
            $getEndPointDeliveryResponse->getEndPointDeliveryResult()[0]->endpointAddress()
        );

        $this->assertEquals(
            $data['getEndPointDeliveryResult']['item#1']['Endpoint_contact'],
            $getEndPointDeliveryResponse->getEndPointDeliveryResult()[0]->endpointContact()
        );

        $this->assertEquals(
            $data['getEndPointDeliveryResult']['item#1']['ShippingAgentCode'],
            $getEndPointDeliveryResponse->getEndPointDeliveryResult()[0]->shippingAgentCode()
        );

        $this->assertEquals(
            $data['getEndPointDeliveryResult']['item#1']['City'],
            $getEndPointDeliveryResponse->getEndPointDeliveryResult()[0]->city()
        );
    }
}