<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Manual;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Manual\GetShipmentDatesResponse;
use LaptopDev\MerlionApi\Response\Result\ShipmentDatesResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetShipmentDatesResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getShipmentDatesResult' => [
                'item#1' => [
                    'Date' => 'test_Date',
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getShipmentDatesResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getShipmentDatesResponse = $this->serializer->deserialize(
            $serialized,
            GetShipmentDatesResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getShipmentDatesResponse->getShipmentDatesResult()
        );

        $this->assertInstanceOf(
            ShipmentDatesResult::class,
            $getShipmentDatesResponse->getShipmentDatesResult()[0]
        );

        $this->assertInstanceOf(
            ShipmentDatesResult::class,
            $getShipmentDatesResponse->getShipmentDatesResult()[1]
        );

        $this->assertEquals(
            $data['getShipmentDatesResult']['item#1']['Date'],
            $getShipmentDatesResponse->getShipmentDatesResult()[0]->date()
        );
    }
}