<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Manual;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Manual\GetShipmentMethodsResponse;
use LaptopDev\MerlionApi\Response\Result\ShipmentMethodsResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetShipmentMethodsResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getShipmentMethodsResult' => [
                'item#1' => [
                    'Code' => 'test_Code',
                    'Description' => 'test_Description',
                    'IsDefault' => 1,
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getShipmentMethodsResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getShipmentMethodsResponse = $this->serializer->deserialize(
            $serialized,
            GetShipmentMethodsResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getShipmentMethodsResponse->getShipmentMethodsResult()
        );

        $this->assertInstanceOf(
            ShipmentMethodsResult::class,
            $getShipmentMethodsResponse->getShipmentMethodsResult()[0]
        );

        $this->assertInstanceOf(
            ShipmentMethodsResult::class,
            $getShipmentMethodsResponse->getShipmentMethodsResult()[1]
        );

        $this->assertEquals(
            $data['getShipmentMethodsResult']['item#1']['Code'],
            $getShipmentMethodsResponse->getShipmentMethodsResult()[0]->code()
        );

        $this->assertEquals(
            $data['getShipmentMethodsResult']['item#1']['Description'],
            $getShipmentMethodsResponse->getShipmentMethodsResult()[0]->description()
        );

        $this->assertEquals(
            $data['getShipmentMethodsResult']['item#1']['IsDefault'],
            $getShipmentMethodsResponse->getShipmentMethodsResult()[0]->isDefault()
        );
    }
}