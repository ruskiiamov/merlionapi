<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Manual;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Manual\GetCertificatesResponse;
use LaptopDev\MerlionApi\Response\Result\CertificatesResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetCertificatesResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getCertificatesResult' => [
                'item#1' => [
                    'Link' => 'test_Link',
                    'ItemNo' => 'test_ItemNo',
                    'CertCode' => 'test_CertCode',
                    'AttachNum' => 123,
                    'ItemName' => 'test_ItemName',
                    'CertName' => 'test_CertName',
                    'Issued' => 'test_Issued',
                    'Expires' => 'test_Expires',
                    'Issuer' => 'test_Issuer',
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getCertificatesResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getCertificatesResponse = $this->serializer->deserialize(
            $serialized,
            GetCertificatesResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getCertificatesResponse->getCertificatesResult()
        );

        $this->assertInstanceOf(
            CertificatesResult::class,
            $getCertificatesResponse->getCertificatesResult()[0]
        );

        $this->assertInstanceOf(
            CertificatesResult::class,
            $getCertificatesResponse->getCertificatesResult()[1]
        );

        $this->assertEquals(
            $data['getCertificatesResult']['item#1']['Link'],
            $getCertificatesResponse->getCertificatesResult()[0]->link()
        );

        $this->assertEquals(
            $data['getCertificatesResult']['item#1']['ItemNo'],
            $getCertificatesResponse->getCertificatesResult()[0]->itemNo()
        );

        $this->assertEquals(
            $data['getCertificatesResult']['item#1']['CertCode'],
            $getCertificatesResponse->getCertificatesResult()[0]->certCode()
        );

        $this->assertEquals(
            $data['getCertificatesResult']['item#1']['AttachNum'],
            $getCertificatesResponse->getCertificatesResult()[0]->attachNum()
        );

        $this->assertEquals(
            $data['getCertificatesResult']['item#1']['ItemName'],
            $getCertificatesResponse->getCertificatesResult()[0]->itemName()
        );

        $this->assertEquals(
            $data['getCertificatesResult']['item#1']['CertName'],
            $getCertificatesResponse->getCertificatesResult()[0]->certName()
        );

        $this->assertEquals(
            $data['getCertificatesResult']['item#1']['Issued'],
            $getCertificatesResponse->getCertificatesResult()[0]->issued()
        );

        $this->assertEquals(
            $data['getCertificatesResult']['item#1']['Expires'],
            $getCertificatesResponse->getCertificatesResult()[0]->expires()
        );

        $this->assertEquals(
            $data['getCertificatesResult']['item#1']['Issuer'],
            $getCertificatesResponse->getCertificatesResult()[0]->issuer()
        );
    }
}