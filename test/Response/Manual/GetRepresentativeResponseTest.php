<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Manual;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Manual\GetRepresentativeResponse;
use LaptopDev\MerlionApi\Response\Result\RepresentativeResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetRepresentativeResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getRepresentativeResult' => [
                'item#1' => [
                    'Representative' => 'test_Representative',
                    'CounterAgentCode' => 'test_CounterAgentCode',
                    'StartDate' => 'test_StartDate',
                    'EndDate' => 'test_EndDate',
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getRepresentativeResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getRepresentativeResponse = $this->serializer->deserialize(
            $serialized,
            GetRepresentativeResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getRepresentativeResponse->getRepresentativeResult()
        );

        $this->assertInstanceOf(
            RepresentativeResult::class,
            $getRepresentativeResponse->getRepresentativeResult()[0]
        );

        $this->assertInstanceOf(
            RepresentativeResult::class,
            $getRepresentativeResponse->getRepresentativeResult()[1]
        );

        $this->assertEquals(
            $data['getRepresentativeResult']['item#1']['Representative'],
            $getRepresentativeResponse->getRepresentativeResult()[0]->representative()
        );

        $this->assertEquals(
            $data['getRepresentativeResult']['item#1']['CounterAgentCode'],
            $getRepresentativeResponse->getRepresentativeResult()[0]->counterAgentCode()
        );

        $this->assertEquals(
            $data['getRepresentativeResult']['item#1']['StartDate'],
            $getRepresentativeResponse->getRepresentativeResult()[0]->startDate()
        );

        $this->assertEquals(
            $data['getRepresentativeResult']['item#1']['EndDate'],
            $getRepresentativeResponse->getRepresentativeResult()[0]->endDate()
        );
    }
}