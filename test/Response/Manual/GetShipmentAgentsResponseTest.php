<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Manual;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Manual\GetShipmentAgentsResponse;
use LaptopDev\MerlionApi\Response\Result\DictionaryResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetShipmentAgentsResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getShipmentAgentsResult' => [
                'item#1' => [
                    'Code' => 'test_Code',
                    'Description' => 'test_Description',
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getShipmentAgentsResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getShipmentAgentsResponse = $this->serializer->deserialize(
            $serialized,
            GetShipmentAgentsResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getShipmentAgentsResponse->getShipmentAgentsResult()
        );

        $this->assertInstanceOf(
            DictionaryResult::class,
            $getShipmentAgentsResponse->getShipmentAgentsResult()[0]
        );

        $this->assertInstanceOf(
            DictionaryResult::class,
            $getShipmentAgentsResponse->getShipmentAgentsResult()[1]
        );

        $this->assertEquals(
            $data['getShipmentAgentsResult']['item#1']['Code'],
            $getShipmentAgentsResponse->getShipmentAgentsResult()[0]->code()
        );

        $this->assertEquals(
            $data['getShipmentAgentsResult']['item#1']['Description'],
            $getShipmentAgentsResponse->getShipmentAgentsResult()[0]->description()
        );
    }
}