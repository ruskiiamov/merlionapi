<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Manual;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Manual\GetCounterAgentResponse;
use LaptopDev\MerlionApi\Response\Result\DictionaryResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetCounterAgentResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getCounterAgentResult' => [
                'item#1' => [
                    'Code' => 'test_Code',
                    'Description' => 'test_Description',
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getCounterAgentResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getCounterAgentResponse = $this->serializer->deserialize(
            $serialized,
            GetCounterAgentResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getCounterAgentResponse->getCounterAgentResult()
        );

        $this->assertInstanceOf(
            DictionaryResult::class,
            $getCounterAgentResponse->getCounterAgentResult()[0]
        );

        $this->assertInstanceOf(
            DictionaryResult::class,
            $getCounterAgentResponse->getCounterAgentResult()[1]
        );

        $this->assertEquals(
            $data['getCounterAgentResult']['item#1']['Code'],
            $getCounterAgentResponse->getCounterAgentResult()[0]->code()
        );

        $this->assertEquals(
            $data['getCounterAgentResult']['item#1']['Description'],
            $getCounterAgentResponse->getCounterAgentResult()[0]->description()
        );
    }
}