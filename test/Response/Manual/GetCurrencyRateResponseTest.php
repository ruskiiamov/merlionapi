<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Manual;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Manual\GetCurrencyRateResponse;
use LaptopDev\MerlionApi\Response\Result\CurrencyRateResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetCurrencyRateResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getCurrencyRateResult' => [
                'item#1' => [
                    'Code' => 'test_Code',
                    'Date' => 'test_Date',
                    'ExchangeRate' => 73.45,
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getCurrencyRateResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getCurrencyRateResponse = $this->serializer->deserialize(
            $serialized,
            GetCurrencyRateResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getCurrencyRateResponse->getCurrencyRateResult()
        );

        $this->assertInstanceOf(
            CurrencyRateResult::class,
            $getCurrencyRateResponse->getCurrencyRateResult()[0]
        );

        $this->assertInstanceOf(
            CurrencyRateResult::class,
            $getCurrencyRateResponse->getCurrencyRateResult()[1]
        );

        $this->assertEquals(
            $data['getCurrencyRateResult']['item#1']['Code'],
            $getCurrencyRateResponse->getCurrencyRateResult()[0]->code()
        );

        $this->assertEquals(
            $data['getCurrencyRateResult']['item#1']['Date'],
            $getCurrencyRateResponse->getCurrencyRateResult()[0]->date()
        );

        $this->assertEquals(
            $data['getCurrencyRateResult']['item#1']['ExchangeRate'],
            $getCurrencyRateResponse->getCurrencyRateResult()[0]->exchangeRate()
        );
    }
}