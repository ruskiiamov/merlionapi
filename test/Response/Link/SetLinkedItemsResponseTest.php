<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Link;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Link\SetLinkedItemsResponse;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class SetLinkedItemsResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'setLinkedItemsResult' => 'test_result',
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'setLinkedItemsResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $setLinkedItemsResponse = $this->serializer->deserialize(
            $serialized,
            SetLinkedItemsResponse::class,
            'soap'
        );


        $this->assertEquals(
            $data['setLinkedItemsResult'],
            $setLinkedItemsResponse->setLinkedItemsResult()
        );
    }
}