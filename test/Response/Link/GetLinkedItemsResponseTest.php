<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Response\Link;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Response\Link\GetLinkedItemsResponse;
use LaptopDev\MerlionApi\Response\Result\LinkedItemsResult;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetLinkedItemsResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'getLinkedItemsResult' => [
                'item#1' => [
                    'No' => 'test_No',
                    'Cust_ItemNo' => 'test_Cust_ItemNo',
                    'Cust_ItemName' => 'test_Cust_ItemName',
                ],
                'item#2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'getLinkedItemsResponse',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $getLinkedItemsResponse = $this->serializer->deserialize(
            $serialized,
            GetLinkedItemsResponse::class,
            'soap'
        );

        $this->assertCount(
            2,
            $getLinkedItemsResponse->getLinkedItemsResult()
        );

        $this->assertInstanceOf(
            LinkedItemsResult::class,
            $getLinkedItemsResponse->getLinkedItemsResult()[0]
        );

        $this->assertInstanceOf(
            LinkedItemsResult::class,
            $getLinkedItemsResponse->getLinkedItemsResult()[1]
        );

        $this->assertEquals(
            $data['getLinkedItemsResult']['item#1']['No'],
            $getLinkedItemsResponse->getLinkedItemsResult()[0]->no()
        );

        $this->assertEquals(
            $data['getLinkedItemsResult']['item#1']['Cust_ItemNo'],
            $getLinkedItemsResponse->getLinkedItemsResult()[0]->custItemNo()
        );

        $this->assertEquals(
            $data['getLinkedItemsResult']['item#1']['Cust_ItemName'],
            $getLinkedItemsResponse->getLinkedItemsResult()[0]->custItemName()
        );
    }
}