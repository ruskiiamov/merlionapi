<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Client;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Client\Catalog;
use LaptopDev\MerlionApi\Client\Client;
use LaptopDev\MerlionApi\Request\Catalog\GetCatalogRequest;
use LaptopDev\MerlionApi\Request\Catalog\GetItemsAvailRequest;
use LaptopDev\MerlionApi\Request\Catalog\GetItemsBarcodesRequest;
use LaptopDev\MerlionApi\Request\Catalog\GetItemsRequest;
use LaptopDev\MerlionApi\Response\Catalog\GetCatalogResponse;
use LaptopDev\MerlionApi\Response\Catalog\GetItemsAvailResponse;
use LaptopDev\MerlionApi\Response\Catalog\GetItemsBarcodesResponse;
use LaptopDev\MerlionApi\Response\Catalog\GetItemsResponse;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;

class CatalogTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /** @var ClientInterface */
    protected $httpClient;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var string */
    protected $clientCode;

    /** @var string */
    protected $login;

    /** @var string */
    protected $password;

    protected function setUp(): void
    {
        $this->httpClient = Mockery::mock(ClientInterface::class);
        $this->serializer = Mockery::mock(SerializerInterface::class);
        $this->clientCode = 'test_client_code';
        $this->login = 'test_login';
        $this->password = 'test_password';
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetCatalogRequest(): void
    {
        $request = Mockery::mock(GetCatalogRequest::class);
        $response = Mockery::mock(GetCatalogResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->clientCode,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetCatalogResponse::class,
            $catalog->sendGetCatalogRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetItemsRequest(): void
    {
        $request = Mockery::mock(GetItemsRequest::class);
        $response = Mockery::mock(GetItemsResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->clientCode,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetItemsResponse::class,
            $catalog->sendGetItemsRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetItemsAvailRequest(): void
    {
        $request = Mockery::mock(GetItemsAvailRequest::class);
        $response = Mockery::mock(GetItemsAvailResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->clientCode,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetItemsAvailResponse::class,
            $catalog->sendGetItemsAvailRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetItemsBarcodesRequest(): void
    {
        $request = Mockery::mock(GetItemsBarcodesRequest::class);
        $response = Mockery::mock(GetItemsBarcodesResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->clientCode,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetItemsBarcodesResponse::class,
            $catalog->sendGetItemsBarcodesRequest($request)
        );
    }
}