<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Client;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Client\Client;
use LaptopDev\MerlionApi\Client\Product;
use LaptopDev\MerlionApi\Request\Product\GetItemsImagesRequest;
use LaptopDev\MerlionApi\Request\Product\GetItemsPropertiesRequest;
use LaptopDev\MerlionApi\Request\Product\GetItemsPropertiesSectionsRequest;
use LaptopDev\MerlionApi\Response\Product\GetItemsImagesResponse;
use LaptopDev\MerlionApi\Response\Product\GetItemsPropertiesResponse;
use LaptopDev\MerlionApi\Response\Product\GetItemsPropertiesSectionsResponse;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;

class ProductTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /** @var ClientInterface */
    protected $httpClient;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var string */
    protected $clientCode;

    /** @var string */
    protected $login;

    /** @var string */
    protected $password;

    protected function setUp(): void
    {
        $this->httpClient = Mockery::mock(ClientInterface::class);
        $this->serializer = Mockery::mock(SerializerInterface::class);
        $this->clientCode = 'test_client_code';
        $this->login = 'test_login';
        $this->password = 'test_password';
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetItemsPropertiesRequest(): void
    {
        $request = Mockery::mock(GetItemsPropertiesRequest::class);
        $response = Mockery::mock(GetItemsPropertiesResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $product = new Product(
            $this->httpClient,
            $this->serializer,
            $this->clientCode,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetItemsPropertiesResponse::class,
            $product->sendGetItemsPropertiesRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetItemsPropertiesSectionsRequest(): void
    {
        $request = Mockery::mock(GetItemsPropertiesSectionsRequest::class);
        $response = Mockery::mock(GetItemsPropertiesSectionsResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $product = new Product(
            $this->httpClient,
            $this->serializer,
            $this->clientCode,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetItemsPropertiesSectionsResponse::class,
            $product->sendGetItemsPropertiesSectionsRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetItemsImagesRequest(): void
    {
        $request = Mockery::mock(GetItemsImagesRequest::class);
        $response = Mockery::mock(GetItemsImagesResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $product = new Product(
            $this->httpClient,
            $this->serializer,
            $this->clientCode,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetItemsImagesResponse::class,
            $product->sendGetItemsImagesRequest($request)
        );
    }
}