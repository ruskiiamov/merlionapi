<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi;

use GuzzleHttp\Client as HttpClient;
use LaptopDev\MerlionApi\Client\Catalog;
use LaptopDev\MerlionApi\Client\Link;
use LaptopDev\MerlionApi\Client\Manual;
use LaptopDev\MerlionApi\Client\Order;
use LaptopDev\MerlionApi\Client\Product;
use LaptopDev\MerlionApi\MerlionApiClient;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;

class MerlionApiClientTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testConstruct(): void
    {
        Mockery::mock('overload:' . Catalog::class);
        Mockery::mock('overload:' . Product::class);
        Mockery::mock('overload:' . Order::class);
        Mockery::mock('overload:' . Link::class);
        Mockery::mock('overload:' . Manual::class);

        $baseUri = 'test_uri';
        $timeout = 10;

        Mockery::mock('overload:' . HttpClient::class)
            ->shouldReceive('__construct')
            ->once()
            ->with([
                'base_uri' => $baseUri,
                'timeout' => $timeout
            ]);

        $merlionApiClient = new MerlionApiClient(
            $baseUri,
            'test_client_code',
            'test_login',
            'test_password',
            $timeout
        );

        $this->assertInstanceOf(
            Catalog::class,
            $merlionApiClient->catalog
        );

        $this->assertInstanceOf(
            Product::class,
            $merlionApiClient->product
        );

        $this->assertInstanceOf(
            Order::class,
            $merlionApiClient->order
        );

        $this->assertInstanceOf(
            Link::class,
            $merlionApiClient->link
        );

        $this->assertInstanceOf(
            Manual::class,
            $merlionApiClient->manual
        );
    }
}