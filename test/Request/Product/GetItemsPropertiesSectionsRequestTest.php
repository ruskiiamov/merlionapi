<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Product;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Product\GetItemsPropertiesSectionsRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetItemsPropertiesSectionsRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'id' => 'test_id',
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'getItemsPropertiesSections',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new GetItemsPropertiesSectionsRequest;
        $request->setId($data['id']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}