<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Order\GetCommandResultDetailsRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetCommandResultDetailsRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'operation_no' => 'test_operation_no',
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'getCommandResultDetails',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new GetCommandResultDetailsRequest;
        $request
            ->setOperationNo($data['operation_no']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}