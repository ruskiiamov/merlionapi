<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Order\SetSignOrderCommandRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class SetSignOrderCommandRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'document_no' => 'test_document_no',
            'sign_type' => 1,
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'setSignOrderCommand',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new SetSignOrderCommandRequest;
        $request
            ->setDocumentNo($data['document_no'])
            ->setSignType($data['sign_type']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}