<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Order\SetOrderLineCommandRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class SetOrderLineCommandRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'document_no' => 'test_document_no',
            'item_no' => 'test_item_no',
            'qty' => 500,
            'price' => 123.4,
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'setOrderLineCommand',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new SetOrderLineCommandRequest;
        $request
            ->setDocumentNo($data['document_no'])
            ->setItemNo($data['item_no'])
            ->setQty($data['qty'])
            ->setPrice($data['price']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}