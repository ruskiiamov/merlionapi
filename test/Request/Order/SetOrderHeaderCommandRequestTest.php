<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Order\SetOrderHeaderCommandRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class SetOrderHeaderCommandRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'document_no' => 'test_document_no',
            'shipment_method' => 'test_shipment_method',
            'shipment_date' => 'test_shipment_date',
            'counter_agent' => 'test_counter_agent',
            'shipment_agent' => 'test_shipment_agent',
            'end_customer' => 'test_end_customer',
            'comment' => 'test_comment',
            'representative' => 'test_representative',
            'endpoint_delivery_id' => 123,
            'packing_type' => 'test_packing_type',
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'setOrderHeaderCommand',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new SetOrderHeaderCommandRequest;
        $request
            ->setDocumentNo($data['document_no'])
            ->setShipmentMethod($data['shipment_method'])
            ->setShipmentDate($data['shipment_date'])
            ->setCounterAgent($data['counter_agent'])
            ->setShipmentAgent($data['shipment_agent'])
            ->setEndCustomer($data['end_customer'])
            ->setComment($data['comment'])
            ->setRepresentative($data['representative'])
            ->setEndpointDeliveryId($data['endpoint_delivery_id'])
            ->setPackingType($data['packing_type']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}