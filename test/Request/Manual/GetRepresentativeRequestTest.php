<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Manual;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Manual\GetRepresentativeRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetRepresentativeRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'CounterAgentCode' => 'test_CounterAgentCode',
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'getRepresentative',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new GetRepresentativeRequest;
        $request
            ->setCounterAgentCode($data['CounterAgentCode']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}