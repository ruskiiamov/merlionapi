<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Manual;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Manual\GetEndPointDeliveryRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetEndPointDeliveryRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'id' => 123,
            'ShippingAgentCode' => 'test_ShippingAgentCode',
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'getEndPointDelivery',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new GetEndPointDeliveryRequest;
        $request
            ->setId($data['id'])
            ->setShippingAgentCode($data['ShippingAgentCode']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}