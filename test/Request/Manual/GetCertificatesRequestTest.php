<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Manual;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Manual\GetCertificatesRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetCertificatesRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'item_id' => 123,
            'page' => 321,
            'rows_on_page' => 20,
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'getCertificates',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new GetCertificatesRequest;
        $request
            ->setItemId($data['item_id'])
            ->setPage($data['page'])
            ->setRowsOnPage($data['rows_on_page']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}