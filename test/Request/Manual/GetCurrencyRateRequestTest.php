<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Manual;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Manual\GetCurrencyRateRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetCurrencyRateRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'date' => 'test_date',
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'getCurrencyRate',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new GetCurrencyRateRequest;
        $request
            ->setDate($data['date']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}