<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Catalog;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Catalog\GetItemsAvailRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetItemsAvailRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'cat_id' => 'test_cat_id',
            'shipment_method' => 'test_shipment_method',
            'shipment_date' => '2021-11-19 22:22:22',
            'only_avail' => '0',
            'item_id' => [
                'item#1' => 'test_code1',
                'item#2' => 'test_code2',
            ],
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'getItemsAvail',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new GetItemsAvailRequest;
        $request
            ->setCatId($data['cat_id'])
            ->setShipmentMethod($data['shipment_method'])
            ->setShipmentDate($data['shipment_date'])
            ->setOnlyAvail($data['only_avail'])
            ->addItemId($data['item_id']['item#1'])
            ->addItemId($data['item_id']['item#2']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}