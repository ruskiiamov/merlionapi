<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Catalog;

use DateTime;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Catalog\GetItemsRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetItemsRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'cat_id' => 'test_cat_id',
            'item_id' => [
                'item#1' => 'test_code1',
                'item#2' => 'test_code2',
            ],
            'shipment_method' => 'test_shipment_method',
            'page' => 45,
            'rows_on_page' => 20,
            'last_time_change' => '2021-11-19T23:21:00'
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'getItems',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new GetItemsRequest;
        $request
            ->setCatId($data['cat_id'])
            ->addItemId($data['item_id']['item#1'])
            ->addItemId($data['item_id']['item#2'])
            ->setShipmentMethod($data['shipment_method'])
            ->setPage($data['page'])
            ->setRowsOnPage($data['rows_on_page'])
            ->setLastTimeChange(
                new DateTime($data['last_time_change'])
            );

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}