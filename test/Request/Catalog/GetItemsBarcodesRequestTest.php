<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Catalog;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Catalog\GetItemsBarcodesRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetItemsBarcodesRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'cat_id' => 'test_cat_id',
            'item_id' => [
                'item#1' => 'test_code1',
                'item#2' => 'test_code2',
            ],
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'getItemsBarcodes',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new GetItemsBarcodesRequest;
        $request
            ->setCatId($data['cat_id'])
            ->addItemId($data['item_id']['item#1'])
            ->addItemId($data['item_id']['item#2']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}