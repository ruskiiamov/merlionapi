<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Catalog;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Catalog\GetCatalogRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetCatalogRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'cat_id' => 'test_cat_id',
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'getCatalog',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new GetCatalogRequest;
        $request
            ->setCatId($data['cat_id']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}