<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Link;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Link\SetLinkedItemsRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class SetLinkedItemsRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'item_id' => 'test_item_id',
            'custitem_id' => 'test_custitem_id',
            'custitem_name' => 'test_custitem_name',
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'setLinkedItems',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new SetLinkedItemsRequest;
        $request
            ->setItemId($data['item_id'])
            ->setCustItemId($data['custitem_id'])
            ->setCustItemName($data['custitem_name']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}