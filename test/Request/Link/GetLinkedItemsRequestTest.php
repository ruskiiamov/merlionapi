<?php

declare(strict_types=1);

namespace LaptopDev\test\MerlionApi\Request\Link;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MerlionApi\Request\Link\GetLinkedItemsRequest;
use LaptopDev\test\MerlionApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\MerlionApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetLinkedItemsRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'page' => 50,
            'rows_on_page' => 100,
            'type_item_id' => 1,
            'item_id' => [
                'item#1' => 'test_item_id1',
                'item#2' => 'test_item_id2',
            ],
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'getLinkedItems',
            $data,
            'https://api.merlion.com/dl/mlservice3',
            'ns1'
        );

        $request = new GetLinkedItemsRequest;
        $request
            ->setPage($data['page'])
            ->setRowsOnPage($data['rows_on_page'])
            ->setTypeItemId($data['type_item_id'])
            ->addItemId($data['item_id']['item#1'])
            ->addItemId($data['item_id']['item#2']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}